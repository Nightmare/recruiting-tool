<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form ActiveForm */
\app\assets\FeedbackAsset::register($this);
?>
<div class="feedback col-lg-5">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'class' => 'form-horizontal',
        ]
    ]); ?>
    <fieldset>
        <legend>Feedback for James Bond</legend>
        <?= $form->field($model, 'met_requirements')->radioList(Feedback::getRequirements()); ?>
        <?= $form->field($model, 'hesitation')->textarea(); ?>
        <?= $form->field($model, 'professional_level')->radioList(Feedback::getSkillLevels()); ?>
        <?= $form->field($model, 'adequacy')->radioList(Feedback::getAdequacyList()); ?>
        <?= $form->field($model, 'adequacy_comment')->textarea(); ?>
        <?= $form->field($model, 'potential_cooperation')->radioList(Feedback::getPotentialCooperation()); ?>
        <?= $form->field($model, 'potential_cooperation_comment'); ?>
        <?= $form->field($model, 'english_level')->radioList(Feedback::getEnglishLevels()); ?>
        <?= $form->field($model, 'general_feedback')->textarea(['rows' => 5]); ?>

        <div class="form-group text-right">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    </fieldset>
    <?php ActiveForm::end(); ?>

</div><!-- feedback -->
