<?php
use kartik\widgets\ActiveForm;
use yii\tagsinput\TagsInput;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;

/** @var app\models\Interview $model */
$form = ActiveForm::begin([
    'action' => Url::to('/interview/interview-complete'),
    'id' => $model->formName(),
    'enableClientValidation' => false,
    'options' => [
        'class' => 'clearfix',
    ]
]); ?>
<?= Html::activeHiddenInput($model, 'candidate_id'); ?>
<?= Html::activeHiddenInput($model, 'interview_id'); ?>
<?= $form->field($model, 'participants')->widget(TagsInput::className(), [
    'clientOptions' => [
        'typeaheadjs' => [
            'displayKey' => 'full_name',
            'valueKey' => 'full_name',
            'source' => new JsExpression('(function () {
                var users = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace(\'full_name\'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: \'/user/list?term=%QUERY\',
                        filter: function (list) {
                            return $.map(list.data, function (user) {
                                return user;
                            });
                        }
                    }
                });
                users.initialize();
                return users.ttAdapter();
            })()'),
        ],
    ]
]); ?>

<?= $form->field($model, 'content')->textarea(['rows' => 4]); ?>

<?php ActiveForm::end(); ?>

<blockquote>
    <small>Just use placeholder <strong><em>#link</em></strong> for place feedback form's link, or raw text <?= Url::to(['interview/feedback', 'id' => $model->interview_id], true); ?></small>
</blockquote>