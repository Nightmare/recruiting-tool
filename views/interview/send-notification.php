<?php
use kartik\widgets\ActiveForm;
use yii\tagsinput\TagsInput;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;

/** @var app\models\Interview $model */
$form = ActiveForm::begin([
    'action' => Url::to('/interview/send-notification'),
    'id' => $model->formName(),
    'enableClientValidation' => false,
    'options' => [
        'class' => 'clearfix',
    ]
]); ?>
<?= Html::activeHiddenInput($model, 'candidate_id'); ?>
<?= $form->field($model, 'participants')->widget(TagsInput::className(), [
    'clientOptions' => [
        'typeaheadjs' => [
            'displayKey' => 'full_name',
            'valueKey' => 'full_name',
            'source' => new JsExpression('(function () {
                var users = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace(\'full_name\'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: \'/user/list?term=%QUERY\',
                        filter: function (list) {
                            return $.map(list.data, function (user) {
                                return user;
                            });
                        }
                    }
                });
                users.initialize();
                return users.ttAdapter();
            })()'),
        ],
    ]
]); ?>

<?= $form->field($model, 'comment')->textarea(['rows' => 3]); ?>
<?= Html::activeCheckbox($model, 'attach_cv'); ?>

<?php ActiveForm::end(); ?>
