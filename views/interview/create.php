<?php
use kartik\widgets\ActiveForm;
use app\widgets\TypeAhead;
use dosamigos\typeahead\Bloodhound;
use yii\tagsinput\TagsInput;
use yii\helpers\Url;
use kartik\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\web\JsExpression;

$accountEngine = new Bloodhound([
    'name' => 'accountEngine',
    'clientOptions' => [
        'datumTokenizer' => new JsExpression('Bloodhound.tokenizers.obj.whitespace(\'name\')'),
        'queryTokenizer' => new JsExpression('Bloodhound.tokenizers.whitespace'),
        'remote' => [
            'url' => Url::to(['/account/list', 'query' => 'QRY']),
            'wildcard' => 'QRY',
            'filter' => new JsExpression('function (list) {
                return $.map(list.data, function (account) {
                    return account;
                });
            }')
        ]
    ]
]);

$vacancyEngine = new Bloodhound([
    'name' => 'vacancyEngine',
    'clientOptions' => [
        'datumTokenizer' => new JsExpression('Bloodhound.tokenizers.obj.whitespace(\'name\')'),
        'queryTokenizer' => new JsExpression('Bloodhound.tokenizers.whitespace'),
        'remote' => [
            'url' => Url::to(['/vacancy/list', 'query' => 'QRY']),
            'wildcard' => 'QRY',
            'filter' => new JsExpression('function (list) {
                return $.map(list.data, function (account) {
                    return account;
                });
            }')
        ]
    ]
]);

/** @var app\models\Interview $model */
$form = ActiveForm::begin([
    'action' => Url::to('/interview/create'),
    'id' => $model->formName(),
    'enableClientValidation' => false,
    'options' => [
        'class' => 'clearfix',
    ]
]); ?>

<?= Html::activeHiddenInput($model, 'interview_id'); ?>
<?= Html::activeHiddenInput($model, 'vacancy_id'); ?>
<?= Html::activeHiddenInput($model, 'account_id'); ?>
<?= Html::activeHiddenInput($model, 'candidate_id'); ?>

<div class="row">
    <?= $form->field($model, 'account', ['options' => ['class' => 'form-group col-sm-6 col-md-6 col-lg-6']])->widget(TypeAhead::className(), [
        'options' => ['class' => 'form-control'],
        'engines' => [$accountEngine],
        'events' => [
            'selected' => new JsExpression('function (e, item) {
                $(\'#interview-form :hidden[name*="account_id"]\').val(item.id);
            }'),
        ],
        'clientOptions' => [
            'highlight' => true,
            'minLength' => 2,
        ],
        'dataSets' => [
            [
                'displayKey' => 'name',
                'valueKey' => 'id',
                'source' => $accountEngine->getAdapterScript(),
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'vacancy', ['options' => ['class' => 'form-group col-sm-6 col-md-6 col-lg-6']])->widget(TypeAhead::className(), [
        'options' => ['class' => 'form-control'],
        'engines' => [$vacancyEngine],
        'events' => [
            'selected' => new JsExpression('function (e, item) {
                $(\'#interview-form :hidden[name*="vacancy_id"]\').val(item.id);
            }'),
        ],
        'clientOptions' => [
            'highlight' => true,
            'minLength' => 2,
        ],
        'dataSets' => [
            [
                'displayKey' => 'name',
                'valueKey' => 'name',
                'source' => $vacancyEngine->getAdapterScript(),
            ],
        ],
    ]); ?>
</div>

<?= $form->field($model, 'appointment_at')->widget(DateTimePicker::className(), [
    'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
    'pluginOptions' => [
        'daysOfWeekDisabled' => [0, 6],
        'todayHighlight' => true,
        'autoclose' => true,
        'format' => 'yyyy-mm-dd hh:ii',
    ],
]); ?>

<?= $form->field($model, 'participants')->widget(TagsInput::className(), [
    'clientOptions' => [
        'typeaheadjs' => [
            'displayKey' => 'full_name',
            'valueKey' => 'full_name',
            'source' => new JsExpression('(function () {
                var users = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace(\'full_name\'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: \'/user/list?term=%QUERY\',
                        filter: function (list) {
                            return $.map(list.data, function (user) {
                                return user;
                            });
                        }
                    }
                });
                users.initialize();
                return users.ttAdapter();
            })()'),
        ],
    ]
]); ?>
<?= $form->field($model, 'comment')->textarea(); ?>

<div class="row">
    <div class="col-md-4"><?= Html::activeCheckbox($model, 'send_invitation', ['class' => 'interview-checker']); ?></div>
    <div class="col-md-8"><?= Html::activeCheckbox($model, 'attach_cv', ['class' => 'interview-attach-cv']); ?></div>
</div>

<?php ActiveForm::end(); ?>
