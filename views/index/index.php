<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\LinkPager;
use app\models\Interview;
use app\models\ContactType;
use app\models\Feedback;
use app\models\Account;

/* @var $this yii\web\View */
/* @var $contactType ContactType */
/* @var $candidates yii\data\ActiveDataProvider */
/* @var $candidate app\models\Candidate */
/* @var $skill app\models\CandidateSkill */
/* @var $form ActiveForm */
/* @var $cvModel app\models\Cv */
/* @var $candidateModel app\models\Candidate */
/* @var $interviewForm app\forms\Interview */

$this->title = 'Dashboard';
//$candidates->prepare();
?>
<div class="alerts-box">

</div>
<?php //echo LinkPager::widget([
//    'pagination' => $candidates->getPagination(),
//]);
?>
<ul class="candidate-list">
    <?php foreach ($candidates->getModels() as $candidate) :
        /** @var \app\models\Interview[] $interviews */
        $interviews = $candidate->getInterviews()->orderBy(['created_at' => SORT_DESC])->all(); ?>

        <?php $createdDate = new DateTime($candidate->created_at); ?>
        <li data-candidate-id="<?= $candidate->id; ?>" class="<?php if ($candidate->skills_type !== null) : ?><?= $candidate->skills_type; ?><?php endif; ?>">
            <ul class="interview-list">
            <?php foreach ($interviews as $interview) :
                $interviewDate = new DateTime($interview->appointment_at); ?>
            <li class="<?= $interview->status ?>" data-interview-id="<?= $interview->id ?>">
                <div class="date-holder">
                    <div class="date-holder-inner dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="month-year"><?= $interviewDate->format('M, Y'); ?></span>
                        <span class="day"><?= $interviewDate->format('d'); ?></span>
                        <span class="time"><?= $interviewDate->format('H:i'); ?></span>
                    </div>
                    <?php if ($interview->status !== Interview::STATUS_COMPLETED) : ?>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="" data-toggle="modal" data-target="#interview-modal">Change details</a></li>
<!--                        <li><a href="" data-toggle="modal" data-target="#interview-complete-modal">Mark as completed</a></li>-->
                        <li><a href="" data-toggle="modal" data-target="#send-notification-modal">Send notification</a></li>
                        <li class="divider"></li>
                        <li><?= Html::a('Cancel/Delete', ['/interview/delete', 'id' => $interview->id]); ?></li>
                    </ul>
                    <?php endif ?>
                </div>
            </li>
            <?php endforeach; ?>
            &nbsp;
            </ul>
            <div class="candidate-inner">
                <div class="social-container">
                    <ul>
                        <li class="si si-linkedin"><a href=""><span class="fa fa-linkedin"></span></a></li>
                        <li class="si si-skype"><a href=""><span class="fa fa-skype"></span></a></li>
                        <li class="si si-bitbucket"><a href=""><span class="fa fa-bitbucket"></span></a></li>
                        <li class="si si-github"><a href=""><span class="fa fa-github"></span></a></li>
                        <li class="si si-facebook"><a href=""><span class="fa fa-facebook"></span></a></li>
                        <li class="si si-twitter"><a href=""><span class="fa fa-twitter"></span></a></li>
                        <li class="si si-google-plus"><a href=""><span class="fa fa-google-plus"></span></a></li>
                        <li class="si si-vk"><a href=""><span class="fa fa-vk"></span></a></li>
                    </ul>
                </div>
                <div class="img-container">
                    <?php if ($candidate->logo) : ?>
                        <?= Html::img($candidate->getLogoUrl(), ['alt' => $candidate->getFullName(), 'class' => 'img-circle']); ?>
                    <?php else : ?>
                        <div class="no-logo img-circle">
                            <i class="fa fa-user"></i>
                        </div>
                    <?php endif; ?>

                    <div class="img-action-box img-circle">
                        <ul class="img-action-list">
                            <li data-toggle="tooltip" title="Upload new photo"><a href=""><i class="fa fa-upload"><?= Html::fileInput('Candidate[logo]'); ?></i></a></li>
                            <li data-toggle="tooltip" title="Mark as 'rockstar'"><a data-skills-type="rockstar" href=""><i class="fa fa-thumbs-o-up"></i></a></li>
                            <li data-toggle="tooltip" title="Mark as 'jackpot'"><a data-skills-type="jackpot" href=""><i class="fa fa-thumbs-o-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="info-container">
                <ul class="additional-info pull-right">
                    <li data-toggle="tooltip" title="Conversation history">
                        <a href="" data-toggle="modal" data-target="#comments-modal">
                            <span class="fa fa-comment"></span>
                            <span
                                class="label label-success"><?= $candidate->getConversationHistories()->count(); ?></span>
                        </a>
                    </li>
                    <li class="additional-info-cv" data-toggle="tooltip" title="Update CV list">
                        <a href="" data-toggle="modal" data-target="#cv-modal">
                            <span class="fa fa-paperclip"></span>
                            <span class="label label-success"><?= $candidate->getCvs()->count(); ?></span>
                        </a>
                    </li>
                    <li class="additional-info-contacts" data-toggle="tooltip" title="Update contact list">
                        <a href="" data-toggle="modal" data-target="#contact-modal">
                            <span class="fa fa-link"></span>
                            <span class="label label-success"><?= $candidate->getContacts()->count(); ?></span>
                        </a>
                    </li>
                    <li data-toggle="tooltip" title="Create interview">
                        <a href="" data-toggle="modal" data-target="#interview-modal">
                            <span class="fa fa-users"></span>
                        </a>
                    </li>
                    <li class="additional-info-hostory" data-toggle="tooltip" title="Candidate's history">
                        <a href="" data-toggle="modal" data-target="#cv-modal">
                            <span class="fa fa-info"></span>
                        </a>
                    </li>
                </ul>
                <h3><?= $candidate->getFullName(); ?></h3>


                <ul class="user-info">
                    <li class="user-info-created">
                        <time datetime="<?= $createdDate->format(DateTime::ISO8601); ?>">
                            <?= $createdDate->format('d M, Y'); ?>
                            <span class="fa fa-clock-o"></span> <?= $createdDate->format('H:i'); ?>
                        </time>
                    </li>
                    <li class="item item-experience"><strong>Experience:</strong>
                        <a data-toggle="modal" data-target="#experience-modal" href=""><?= $candidate->experience; ?></a> year(s)
                    </li>
                    <?php if ($candidate->position) : ?>
                    <li><strong>Position:</strong> <?= $candidate->position; ?></li>
                    <?php endif; ?>
                    <li><strong>Recruiter:</strong> <?= $candidate->getRecruiter()->one()->getFullname(); ?></li>
                </ul>

                <?php if ($interviews) : ?>
                <ul class="feedback-info">
                    <?php foreach ($interviews as $interview) :
                        /** @var Feedback $feedback */
                        /** @var Account $account */
                        $account = $interview->getAccount()->one();
                        ?>
                        <?php foreach ($interview->getFeedbacks()->all() as $feedback) : ?>
                    <li class="met-requirement <?= $feedback->met_requirements ?>">
                        <span data-toggle="tooltip" title="Professional level">
                            <i class="fa fa-graduation-cap"></i><?= $feedback->professional_level; ?>
                        </span>
                        <span data-toggle="tooltip" title="English level">
                            <i class="fa fa-language"></i><?= $feedback->english_level; ?>
                        </span>
                        <span data-toggle="tooltip" title="Adequacy">
                            <i class="fa fa-wheelchair"></i><?= $feedback->adequacy; ?>
                        </span>
                        <span data-toggle="tooltip" title="Potential cooperation">
                            <i class="fa fa-recycle"></i><?= $feedback->potential_cooperation; ?>
                        </span>
                        <span data-toggle="tooltip" title="Account name: <?= $account->name; ?>">
                            <i class="fa fa-money"></i><?= $account->name; ?>
                        </span>
                    </li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>

                <div class="user-info-skill">
                    <strong>Skills:</strong>
                    <?php foreach ($candidate->getCandidateSkills()->limit(5)->all() as $skill) : ?><!--
                --><span class="label label-primary"><?= $skill->skill ?></span><!--
                --><?php endforeach; ?>
                    <a href="#" class="lnk-skill-edit" data-toggle="modal" data-target="#skills-modal">
                        <span class="fa fa-tags" data-toggle="tooltip" title="Update skills"></span></a>
                </div>
            </div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
<?= LinkPager::widget([
    'pagination' => $candidates->getPagination(),
]);
?>
<!-- Candidate photo crop modal -->
<?php Modal::begin([
    'id' => 'candidate-photo-modal',
    'header' => '<h4 class="modal-title">Edit candidate photo</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
        . PHP_EOL .
        Html::button('Save changes', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'candidate-photo-modal-label',
        'aria-hidden' => 'true',
    ],
]); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Candidate experience modal -->
<?php Modal::begin([
    'id' => 'experience-modal',
    'header' => '<h4 class="modal-title">Edit candidate experience</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
        . PHP_EOL .
        Html::button('Save changes', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'experience-modal-label',
        'aria-hidden' => 'true',
    ],
]); ?>
<?= Html::input('number', 'Candidate[experience]', 0, ['min' => 0, 'class' => 'form-control']); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Candidate creation modal -->
<?php Modal::begin([
    'id' => 'candidate-modal',
    'header' => '<h4 class="modal-title">Add new candidate</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
        . PHP_EOL .
        Html::button('Add', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'candidate-modal-label',
        'aria-hidden' => 'true',
        'data-url' => Url::to('/candidate/create'),
    ],
]); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Skills modal -->
<?php Modal::begin([
    'id' => 'skills-modal',
    'header' => '<h4 class="modal-title">Edit candidate skills</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
        . PHP_EOL .
        Html::button('Save changes', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'skills-modal-label',
        'aria-hidden' => 'true',
    ],
]); ?>
<?= Html::textInput(false, '', ['class' => 'form-control', 'data-role' => 'tagsinput']); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- CV modal -->
<?php Modal::begin([
    'id' => 'cv-modal',
    'header' => '<h4 class="modal-title">Edit CV list</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'type' => 'button'])
        . PHP_EOL .
        Html::beginTag('button', ['class' => 'btn btn-success btn-modal-add-new', 'type' => 'button']) .
        'Add new' .
        Html::fileInput('Cv[path]') .
        Html::endTag('button'),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'cv-modal-label',
        'aria-hidden' => 'true',
    ],
]); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Contact modal -->
<?php Modal::begin([
    'id' => 'contact-modal',
    'header' => '<h4 class="modal-title">Edit contact list</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'type' => 'button'])
        . PHP_EOL .
        Html::button('Save changes', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'contact-modal-label',
        'aria-hidden' => 'true',
    ],
]); ?>
<h5>Click "contact type" icon to add new contact</h5>
<ul class="modal-contact-menu">
    <?php foreach (ContactType::find()->all() as $contactType) : ?>
    <li data-id="<?= $contactType->id; ?>" data-toggle="tooltip" title="<?= \yii\helpers\Inflector::titleize($contactType->name); ?>" class="si si-<?= str_replace(' ', '-', $contactType->name); ?>">
        <a href="">
            <i class="fa fa-<?= str_replace(' ', '-', $contactType->name === 'email' ? 'at' : $contactType->name); ?>"></i>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
<?= Html::beginForm(['candidate/save-contacts'], 'post', ['class' => 'modal-contact-form clearfix']); ?>
<?= Html::endForm(); ?>
<?php Modal::end() ?>
<!-- # -->


<!-- Interview modal -->
<?php Modal::begin([
    'id' => 'interview-modal',
    'header' => '<h4 class="modal-title">Create/Update interview</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'type' => 'button'])
        . PHP_EOL .
        Html::button('Save interview', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'interview-modal-label',
        'aria-hidden' => 'true',
        'data-url' => Url::to('/interview/create'),
    ],
]); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Send notification modal -->
<?php Modal::begin([
    'id' => 'send-notification-modal',
    'header' => '<h4 class="modal-title">Send notification</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'type' => 'button'])
        . PHP_EOL .
        Html::button('Send notification', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'send-notification-modal-label',
        'aria-hidden' => 'true',
        'data-url' => Url::to('/interview/send-notification'),
    ],
]); ?>
<?php Modal::end() ?>
<!-- # -->

<!-- Interview complete modal -->

<?php /* Modal::begin([
    'id' => 'interview-complete-modal',
    'header' => '<h4 class="modal-title">Send feedback link</h4>',
    'footer' =>
        Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'type' => 'button'])
        . PHP_EOL .
        Html::button('Send feedback link', ['class' => 'btn btn-primary btn-modal-save']),
    'options' => [
        'role' => 'dialog',
        'aria-labelledby' => 'interview-complete-modal-label',
        'aria-hidden' => 'true',
        'data-url' => Url::to('/interview/interview-complete'),
    ],
]); ?>
<?php Modal::end() */ ?>
<!-- # -->

<!-- Comments modal -->
<div class="modal fade" id="comments-modal" tabindex="-1" role="dialog" aria-labelledby="comments-modal-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit candidate skills</h4>
            </div>
            <div class="modal-body">
                <span class="timeline-seperator text-center"><span>10:30PM January 1st, 2013</span></span>

                <!--<div class="chat-body no-padding profile-message">
                    <ul>
                        <li class="message">
                            <img src="img/avatars/sunny.png" class="online">
                                    <span class="message-text"> <a href="javascript:void(0);" class="username">John Doe
                                            <small class="text-muted pull-right ultra-light"> 2 Minutes ago</small>
                                        </a> Can't divide were divide fish forth fish to. Was can't form the, living life grass darkness very
                                        image let unto fowl isn't in blessed fill life yielding above all moved </span>
                            <ul class="list-inline font-xs">
                                <li>
                                    <a href="javascript:void(0);" class="text-info"><i class="fa fa-reply"></i>
                                        Reply</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i>
                                        Like</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-muted">Show All Comments (14)</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-primary">Edit</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-danger">Delete</a>
                                </li>
                            </ul>
                        </li>
                        <li class="message message-reply">
                            <img src="img/avatars/3.png" class="online">
                            <span class="message-text"> <a href="javascript:void(0);" class="username">Serman Syla</a> Haha! Yeah I know what you mean. Thanks for the file Sadi! <i
                                    class="fa fa-smile-o txt-color-orange"></i> </span>

                            <ul class="list-inline font-xs">
                                <li>
                                    <a href="javascript:void(0);" class="text-muted">1 minute ago </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i>
                                        Like</a>
                                </li>
                            </ul>

                        </li>
                        <li class="message message-reply">
                            <img src="img/avatars/4.png" class="online">
                            <span class="message-text"> <a href="javascript:void(0);" class="username">Sadi Orlaf </a> Haha! Yeah I know what you mean. Thanks for the file Sadi! <i
                                    class="fa fa-smile-o txt-color-orange"></i> </span>

                            <ul class="list-inline font-xs">
                                <li>
                                    <a href="javascript:void(0);" class="text-muted">a moment ago </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i>
                                        Like</a>
                                </li>
                            </ul>
                            <input class="form-control input-xs" placeholder="Type and enter" type="text">
                        </li>
                    </ul>
                </div>-->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script type="text/plain" id="tpl-cv">
{{##def.dateFormat:param:
    {{ var DPGlobal = $.fn.datetimepicker.DPGlobal, formatType = 'php'; }}
    {{ dateFormat = DPGlobal.parseFormat(param.dateFormat, formatType); }}
    {{= $.fn.datetimepicker.DPGlobal.formatDate(new Date(param.datetime), dateFormat, 'en', formatType) }}
#}}
    <div class="cv-item" data-id="{{=it.id}}">
        <i class="fa fa-file-text icon-cv"></i> {{=it.path}}
        <div class="pull-right">
            <small class="pull-left">{{#(def.dateFormat:{datetime: it.created_at, dateFormat: 'd M, Y'} || '')}} <i class="fa fa-clock-o"></i> {{#(def.dateFormat:{datetime: it.created_at, dateFormat: 'H:i'} || '')}}</small>
            <a href="<?php echo Url::toRoute('cv/download'); ?>?id={{=it.id}}" class="btn btn-info no-radius btn-xs pull-left"><i class="fa fa-download"></i></a>
            <a href="" class="btn btn-danger no-radius btn-xs pull-left btn-delete"><i class="fa fa-trash-o"></i></a>
        </div>
    </div>

</script>

<script type="text/plain" id="tpl-not-found">
    <h4>No data found.</h4>
</script>

<script type="text/plain" id="tpl-contact-input">
<div class="form-group col-lg-6">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-{{=it.contact_type}}"></i></span>
        <input type="text" name="Contact[{{=it.index}}][name]" value="{{=it.name}}" class="form-control">
        <input type="hidden" name="Contact[{{=it.index}}][contact_type_id]" value="{{=it.contact_type_id}}">
        <span class="input-group-btn">
            <button class="btn btn-default btn-remove" type="button"><i class="fa fa-trash-o"></i></button>
        </span>
    </div>
</div>
</script>

<script type="text/plain" id="tpl-alert">
<div class="bs-component" data-alert-salt="{{=it.salt}}">
    <div class="alert alert-dismissable alert-{{=it.type}}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{~ it.messages :message}}
        <p>{{=message}}</p>
        {{~}}
    </div>
</div>
</script>