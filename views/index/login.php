<?php
use yii\helpers\Html;
use app\assets\LoginAsset;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login fixed">
<?php $this->beginBody() ?>

<div class="wrapper animated flipInY">
    <div class="logo"><a href="#"><i class="fa fa-bolt"></i> <span>Recruiting tool</span></a></div>
    <div class="box">
        <div class="header clearfix">
            <div class="pull-left"><i class="fa fa-sign-in"></i> Sign In</div>
            <?php /*<div class="pull-right"><a href="#"><i class="fa fa-times"></i></a></div>*/ ?>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="alert alert-warning no-radius no-margin padding-sm"><i class="fa fa-info-circle"></i> Please sign in to recruiting tool dashboard</div>
        <div class="box-body padding-md">
            <?= $form->field($model, 'username')->label(false); ?>
            <?= $form->field($model, 'password')->label(false)->passwordInput(); ?>
            <div class="form-group">
                <?= Html::activeCheckbox($model, 'rememberMe'); ?>
                <?= Html::activeLabel($model, 'rememberMe'); ?>
            </div>
            <div class="box-footer">
                <?= Html::submitButton('Sign in', ['class' => 'btn btn-success btn-block']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php /*<div class="box-extra clearfix">
        <a href="#" class="pull-left btn btn-xs">Forgotten Password?</a>
        <a href="register.html" class="pull-right btn  btn-xs">Register an account</a>
    </div> */ ?>

    <footer>
        Copyright &copy; <?= date('Y') ?> by Ivan Zubok.
        <?php /*
        <ul class="list-unstyled list-inline">
            <li><a href="#">Home</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">UI Elements</a></li>
            <li><a href="#">Charts</a></li>
            <li><a href="#">Mobile Apps</a></li>
            <li><a href="#">Contact</a></li>
        </ul>*/ ?>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
