<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([
    'action' => Url::to('candidate/create'),
    'id' => 'candidate-form',
]); ?>
    <div class="clearfix">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(); ?>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>