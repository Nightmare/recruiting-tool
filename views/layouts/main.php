<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

$this->registerMetaTag(['name' => 'application-name', 'content' => 'Recruiting Tool'], 'application-name');
AppAsset::register($this);
$this->registerJs(sprintf('var baseUrl = \'%s\';', rtrim(Yii::$app->homeUrl, '/')), $this::POS_HEAD, 'baseUrl');
$isGuest = Yii::$app->getUser()->getIsGuest();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Recruiting tool',
        'brandUrl' => Yii::$app->getHomeUrl(),
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => 'Dashboard', 'url' => ['/']],
    ];

    if (!$isGuest) {
        $items[] = ['label' => 'Actions', 'url' => 'javascript:void(0);',
            'items' => [
                ['label' => 'Add candidate', 'url' => '#', 'linkOptions' => ['data-toggle' => 'modal', 'data-target' => '#candidate-modal']],
            ],
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $items,
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            $isGuest
                ? ['label' => 'Login', 'url' => ['/login']]
                : ['label' => 'Logout (' . Yii::$app->getUser()->getIdentity()->login . ')', 'url' => ['/logout'], 'linkOptions' => ['data-method' => 'post']],
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content; ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
