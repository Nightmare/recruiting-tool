chrome.browserAction.onClicked.addListener(function (tab) {
    var contactUrl = 'https://www.linkedin.com/contacts/api/contacts/li_[LINKEDIN_ID]/?fields=name,emails_extended,birthday,phone_numbers,sites,addresses,company,title,location,ims,profiles,twitter,wechat,display_sources&_=';

    //get contact info
    chrome.tabs.sendMessage(tab.id, {text: 'report_back'}, function (data) {
        var publicUrl = data.shift() || '',
            skills = data.shift() || [],
            currentPosition = data.shift() || '',
            fullName = data.shift() || '',
            pictureUrl = data.shift() || '',
            workPeriods = data.shift() || [],
            linkedinId = /(\?|&)id=(\d+)&?/i.exec(tab.url),
            replacement = {
                '^http:\/\/': 'https://',
                '\\w+\\.linkedin\\.com': 'www.linkedin.com',
                '\/[a-z]{2}$': '',
                '\/$': ''
            },
            requestData = {
                skills: skills,
                fullName: fullName,
                workPeriods: workPeriods,
                pictureUrl: pictureUrl,
                currentPosition: currentPosition,
                contacts: {}
            };

        if (linkedinId.length) {
            linkedinId = linkedinId[linkedinId.length - 1];
        }

        for (var regex in replacement) {
            if (replacement.hasOwnProperty(regex)) {
                publicUrl = publicUrl.replace(new RegExp(regex, 'i'), replacement[regex]);
            }
        }

        requestData.publicUrl = publicUrl;
        requestData.linkedinId = linkedinId;

        var sendProfile = function (requestData) {
            $.ajax({
                url: 'http://128.199.46.17/candidate/linkedin/import',
                data: requestData,
                dataType: 'json',
                method: 'get',
                statusCode: {
                    401: function () {
                        alert('Please login first');
                        chrome.tabs.create({url: 'http://128.199.46.17/login'}, function (tab) {
                        });
                    }
                },
                success: function (data) {
                    var message;
                    if (!$.isPlainObject(data) || !data.success) {
                        message = 'Something wrong';
                    } else {
                        switch (data.userMessageCode) {
                            case 'success':
                                message = 'User successfully has ben added';
                                break;
                            case 'exist':
                                message = 'User already exist';
                                break;
                            case 'error':
                            default:
                                message = 'Something wrong';
                                break
                        }
                    }

                    alert(message);
                },
                error: function () {
                    console.log(arguments);
                }
            });
        };

        var grabData = function (data, dataContainer, dataField, namedDataField) {
            if (!data.hasOwnProperty(dataContainer)) {
                return;
            }

            namedDataField = namedDataField || dataField;

            for (var i in data[dataContainer]) {
                if (!data[dataContainer].hasOwnProperty(i)) {
                    continue;
                }
                var _insideData = data[dataContainer][i];

                if (_insideData.hasOwnProperty(dataField) && _insideData[dataField]) {
                    if (!requestData.contacts.hasOwnProperty(namedDataField) || !$.isArray(requestData.contacts[namedDataField])) {
                        requestData.contacts[namedDataField] = [];
                    }

                    requestData.contacts[namedDataField].push(_insideData[dataField]);
                }
            }
        };

        try {
            $.getJSON(contactUrl.replace('[LINKEDIN_ID]', linkedinId) + new Date().getTime(), {}, function (data) {
                if ($.isPlainObject(data) && data.status === 'success') {
                    if ($.isPlainObject(data.contact_data) && !$.isEmptyObject(data.contact_data)) {
                        var contactData = data.contact_data;

                        // emails data
                        grabData(contactData, 'emails_extended', 'email');

                        // phone numbers data
                        grabData(contactData, 'phone_numbers', 'number', 'phone');

                        // twitter data
                        grabData(contactData, 'twitter', 'url', 'twitter');
                    }
                }
            })
            .done(function () {
                sendProfile(requestData);
            });
        } catch (e) {
            sendProfile(requestData);
            console.log(e);
        }

        console.log(requestData);
    });
});
