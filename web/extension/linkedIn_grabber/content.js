chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
    if (msg.text === 'report_back') {
        var publicUrlEl = document.querySelector('.view-public-profile'),
            skillsEls = document.querySelectorAll('.endorse-item-name-text'),
            positionEl = document.querySelector('#background-experience > h3 + div[id^="experience-"] h4 a'),
            fullNameEl = document.querySelector('.full-name'),
            profilePicEl = document.querySelector('.profile-picture img'),
            workPeriodEls = document.querySelectorAll('.experience-date-locale'),
            skills = [],
            workPeriods = [],
            i, l, publicUrl, parts, period;

        for (i = 0, l = skillsEls.length; i < l; i++) {
            skills.push(skillsEls[i].innerText);
        }

        if (!publicUrlEl) {
            publicUrlEl = document.querySelector('.public-profile-url');
            publicUrl = 'http://' + (publicUrlEl ? publicUrlEl.innerText : '');
        } else {
            publicUrl = publicUrlEl.href;
        }

        for (i = 0, l = workPeriodEls.length; i < l; i++) {
            parts = /\(.+\)/.exec(workPeriodEls[i].innerText);
            if (!parts.length) {
                continue;
            }

            period = parts.shift();
            workPeriods.push(period.replace('(', '').replace(')', ''));
        }

        var arguments = [
            publicUrl || '',
            skills,
            positionEl ? positionEl.innerText : '',
            fullNameEl ? fullNameEl.innerText : '',
            profilePicEl ? profilePicEl.src : '',
            workPeriods
        ];
        sendResponse(arguments);
    }
});