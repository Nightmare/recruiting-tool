(function (window, document, $, doT, undefined) {
    'use strict';

    var $d = $(document),
        baseUrl = window.baseUrl || '';

    var ajaxForm = function (formSelector) {
        $d
            .on('beforeSubmit', formSelector, function (e) {
                var $form = $(e.target);

                $.post($form.attr('action'), $form.serialize())
                    .done(function (result, status) {
                        if ('success' !== status) {
                            return;
                        }

                        if ($.isPlainObject(result) && result.success === true) {
                            $form.closest('.modal').modal('hide');
                        } else {
                            $form.parent().html(result);
                        }
                    })
                    .fail(function () {
                        console.log('server error');
                        //$('#modal').modal('hide');
                    });

                return false;
            })
            .on('submit', formSelector, function (e) {
                e.preventDefault();
            });

    };

    (function () {
        var renderAlerts = function (data) {
            if (!$.isPlainObject(data) || !$.isPlainObject(data.messages) || $.isEmptyObject(data.messages)) {
                return;
            }

            var messages = data.messages,
                $alertsBox = $('.alerts-box'),
                hideAlert = function ($el) {
                    setTimeout(function () {
                        $el.fadeOut(500).promise().then(function () {
                            $el.remove();
                        });
                    }, 7000);
                };

            for (var type in messages) {
                if (messages.hasOwnProperty(type)) {
                    var _type = type;
                    if ('error' === type) {
                        _type = 'danger';
                    }
                    var salt = Math.random();
                    $alertsBox.tmpl('tpl-alert', {type: _type, messages: messages[type], salt: salt}, true);
                    hideAlert($('.bs-component[data-alert-salt=\'' + salt + '\']'));
                }
            }
        };

        $(document).ajaxComplete(function (e, jqXHR) {
            renderAlerts(jqXHR.responseJSON);
        });
    })();

    // activate tooltips
    (function () {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
    })();


    // extend jQuery for doT.js template
    // @usage: $('#container').tmpl('photo', photos);
    (function () {
        $.fn.tmpl = function (tmplId, data, isAppend) {
            var tmpl,
                method = isAppend ? 'append' : 'html';

            if (($.isArray(data) && !data.length) || ($.isPlainObject(data) && $.isEmptyObject(data))) {
                tmpl = doT.template($('#tpl-not-found').html());
                data = [undefined];
            } else {
                if (!$.isArray(data)) {
                    data = [data];
                }

                tmpl = doT.template($('#' + tmplId).html());
            }

            return this.each(function () {
                var html = '', i, l;

                for (i = 0, l = data.length; i < l; i++) {
                    html += tmpl(data[i]);
                }

                $(this)[method](html);
            });
        };
    })();

    // skills logic
    (function () {
        $d.on('click', '#skills-modal .btn-modal-save', function (e) {
            var $modal = $(this).closest('#skills-modal'),
                candidateId = $modal.data('candidate-id'),
                $container = $('.candidate-list > li[data-candidate-id="' + candidateId + '"]'),
                skills = $(':text', $modal).val().split(','),
                $skillContainer = $('.user-info-skill', $container),
                $linkSkill = $('.lnk-skill-edit', $container),
                i, l;

            $.getJSON(baseUrl + '/skill/update', {skills: skills, candidate_id: candidateId}/*, function () {}*/);

            // remove old labels
            $('.label', $skillContainer).remove();

            for (i = 0, l = skills.length; i < l; i++) {
                $linkSkill.before($('<span/>', {'class': 'label label-primary'}).text($.trim(skills[i])));
            }

            // hide modal window
            $modal.modal('hide');
        });

        // subscribe to modal show event
        $('#skills-modal').on('show.bs.modal', function (e) {
            var $candidateItem = $(e.relatedTarget).closest('li[data-candidate-id]'),
                candidateId = +$candidateItem.data('candidate-id'),
                $modal = $(this).data('candidate-id', candidateId);

            $.getJSON(baseUrl + '/skill/read', {candidate_id: candidateId}, function (result) {
                var i, l;
                if (result.data && result.data.length) {
                    var $input = $('.form-control:text', $modal);

                    // add skill tags
                    for (i = 0, l = result.data.length; i < l; i++) {
                        $input.tagsinput('add', result.data[i]);
                    }
                }
            });
        })
    })();

    // CV logic
    (function () {
        var cvs = {
            _candidateId: undefined,
            _$label: undefined,
            _$container: $('.modal-body', '#cv-modal'),
            data: [],
            _render: function () {
                this._$container.tmpl('tpl-cv', this.data);
                return this;
            },
            setData: function (data) {
                this.data = data;

                return this._render();
            },
            setCandidateId: function (candidateId) {
                this._candidateId = candidateId;
                this._$label = $('.candidate-list > li[data-candidate-id="' + candidateId + '"] .additional-info-cv .label');
                return this;
            },
            getCandidateId: function () {
                return this._candidateId;
            },
            add: function (data) {
                this._$label.text(parseInt(this._$label.text(), 10) + 1);
                this.data.push(data);

                return this._render();
            },
            remove: function (id) {
                for (var i in this.data) {
                    if (this.data.hasOwnProperty(i) && this.data[i].id == id) {
                        this.data.splice(i, 1);
                        break;
                    }
                }

                this._$label.text(parseInt(this._$label.text(), 10) - 1);

                return this._render();
            }
        };

        $('#cv-modal').on('show.bs.modal', function (e) {
            var $candidateItem = $(e.relatedTarget).closest('[data-candidate-id]'),
                $this = $(this);

            cvs.setCandidateId(+$candidateItem.data('candidate-id'));

            // read and display CV list
            $.getJSON(baseUrl + '/cv/read', {candidate_id: cvs.getCandidateId()}, function (result) {
                var $container = $('.modal-body', $this);
                if (result.data) {
                    cvs.setData(result.data);
                }
            });
        });

        // delete CV
        $d.on('click', '#cv-modal .cv-item .btn-delete', function (e) {
            e.preventDefault();

            (function ($item) {
                var id = +$item.data('id');
                $.getJSON(baseUrl + '/cv/delete', {id: id}, function (result) {
                    if (true === result.success) {
                        cvs.remove(id);
                    }
                });
            })($(this).closest('.cv-item'));

        });

        // upload new CV
        $d.on('change', '#cv-modal :file', function () {
            var $this = $(this),
                formData = new FormData();

            formData.append($this.attr('name'), $this.get(0).files[0]);
            formData.append('Cv[candidate_id]', cvs.getCandidateId());

            $.ajax({
                url: baseUrl + '/cv/create',
                type: 'POST',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (result, textStatus, jqXHR) {
                    if (result.success) {
                        cvs.add(result.data);
                    } else {
                        console.log('ERRORS: ' + result.error);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('ERRORS: ' + textStatus);
                }
            });

            $this.replaceWith($this.clone(true));
        });
    })();

    // create candidate
    (function () {

        $('#candidate-modal').on('show.bs.modal', function (e) {
            var $this = $(this);
            $('.modal-body', $this).load($this.data('url'));
        });

        ajaxForm('form#candidate-form');

        $d.on('click', '#candidate-modal .btn-modal-save', function (e) {
            e.preventDefault();
            $(this).closest('#candidate-modal').find('form').trigger('submit');
        });
    })();

    // candidate experience
    (function () {
        $('#experience-modal').on('show.bs.modal', function (e) {
            $('[type="number"]', this)
                .val($(e.relatedTarget).text())
                .data('data-candidate-id', $(e.relatedTarget).closest('[data-candidate-id]').attr('data-candidate-id'));
        });

        $d.on('click', '#experience-modal .btn-modal-save', function (e) {
            e.preventDefault();

            var $this = $(this),
                $modal = $this.closest('#experience-modal'),
                $input = $('[type="number"]', $modal),
                years = parseInt($input.val(), 10),
                candidateId = +$input.data('data-candidate-id');

            years = years < 0 ? 0 : years;

            $.ajax({
                url: baseUrl + '/candidate/' + candidateId,
                data: {'Candidate[experience]': years},
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $('.candidate-list > li[data-candidate-id="' + candidateId + '"] .item-experience a').text(years);
                    }
                }
            });

            $modal.modal('hide');
        });
    })();

    // candidates logo
    (function () {
        var dimensions = null;

        // set candidate type (rockstar or jackpot)
        $d.on('click', '.img-action-box [data-skills-type]', function (e) {
            e.preventDefault();

            var $this = $(this),
                $item = $this.closest('[data-candidate-id]'),
                candidateId = +$item.attr('data-candidate-id'),
                type = $this.data('skills-type');

            // remove current type
            if ($item.hasClass(type)) {
                type = null;
            }

            $.ajax({
                url: baseUrl + '/candidate/' + candidateId,
                type: 'POST',
                data: {'Candidate[skills_type]': type},
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $item.removeClass('rockstar jackpot').addClass(type);
                    }
                }
            });
        });

        // upload logo
        $d.on('change', '.img-action-box :file', function () {
            var $this = $(this),
                formData = new FormData(),
                candidateId = +$this.closest('[data-candidate-id]').attr('data-candidate-id');

            formData.append($this.attr('name'), $this.get(0).files[0]);

            $.ajax({
                url: baseUrl + '/candidate/' + candidateId,
                type: 'POST',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (result, textStatus, jqXHR) {
                    if (result.success) {
                        $('#candidate-photo-modal').data('photo', result.photo).data('candidate_id', candidateId).modal({
                            keyboard: false
                        })
                    }
                }
            });

            $this.replaceWith($this.clone(true));
        });

        $('#candidate-photo-modal').on('show.bs.modal', function () {
            var $this = $(this),
                $container = $('.modal-body', $this),
                $img = $('<img>', {src: $this.data('photo').path});

            $container.html($('<div>', {'class': 'modal-image-crop-box'}).html($img));
            $img.Jcrop({
                aspectRatio: 1,
                onSelect: function (size) {
                    dimensions = size;
                }
            });
        });

        // crop candidate photo
        $d.on('click', '#candidate-photo-modal .btn-modal-save', function (e) {
            e.preventDefault();

            if (null === dimensions) {
                return;
            }

            var $modal = $('#candidate-photo-modal');

            $.getJSON(baseUrl + '/candidate/set-cropped-photo',
                {dimensions: dimensions, candidate_id: $modal.data('candidate_id')},
                function (data) {
                    var $imgContainer = $('.candidate-list > li[data-candidate-id="' + $modal.data('candidate_id') + '"] .img-container'),
                        $img = $('> img', $imgContainer),
                        src = [data.path, Math.random()].join('?');

                    if ($img.length) {
                        $img.attr('src', src);
                    } else {
                        $('> .no-logo', $imgContainer).remove();

                        $imgContainer.append($('<img>', {'class': 'img-circle', src: src}));
                    }
                }
            );

            dimensions = null;
            $modal.modal('hide');
        })
    })();

    // interview-modal
    (function () {
        ajaxForm('form#interview-form');

        $d
            .on('click', '#interview-modal .btn-modal-save', function (e) {
                e.preventDefault();
                var $modal = $(this).closest('#interview-modal'),
                    $form = $modal.find('form');

                $form
                    .attr('action', $form.attr('action').replace(/\?.*/, '') + '?' + $.param({candidate_id: $modal.data('candidate-id')}))
                    .trigger('submit');
            })
            .on('change', '.interview-checker', function () {
                var isChecked = $(this).is(':checked');
                if (!isChecked) {
                    $('.interview-attach-cv').attr('checked', isChecked);
                }
            })
            .on('click', '.interview-attach-cv', function (e) {
                if (!$('.interview-checker').is(':checked')) {
                    e.preventDefault();
                }
            });

        $('#interview-modal').on('shown.bs.modal', function (e) {
            var $this = $(this),
                candidateId = $(e.relatedTarget).closest('[data-candidate-id]').attr('data-candidate-id'),
                interviewId = $(e.relatedTarget).closest('[data-interview-id]').attr('data-interview-id');

            $this.data('candidate-id', candidateId);

            $('.modal-body', $this).load($this.data('url') + '?' + $.param({
                candidate_id: candidateId,
                interview_id: interviewId
            }));
        });
    })();

    // send-notification-modal
    (function () {
        $('#send-notification-modal').on('shown.bs.modal', function (e) {
            var $this = $(this),
                candidateId = $(e.relatedTarget).closest('[data-candidate-id]').attr('data-candidate-id');

            $this.data('candidate-id', candidateId);

            $('.modal-body', $this).load($this.data('url') + '?' + $.param({candidate_id: candidateId}));
        });

        ajaxForm('form#send-notification-form');

        $d.on('click', '#send-notification-modal .btn-modal-save', function (e) {
            e.preventDefault();
            var $modal = $(this).closest('#send-notification-modal'),
                $form = $modal.find('form');

            $form
                .attr('action', $form.attr('action').replace(/\?.*/, '') + '?' + $.param({candidate_id: $modal.data('candidate-id')}))
                .trigger('submit');
        });
    })();

    // interview-complete-modal
    (function () {
        $('#interview-complete-modal').on('shown.bs.modal', function (e) {
            var $this = $(this),
                candidateId = $(e.relatedTarget).closest('[data-candidate-id]').attr('data-candidate-id'),
                interviewId = $(e.relatedTarget).closest('[data-interview-id]').attr('data-interview-id');

            $this.data('candidate-id', candidateId);
            $this.data('interview-id', interviewId);

            $('.modal-body', $this).load($this.data('url') + '?' + $.param({
                candidate_id: candidateId,
                interview_id: interviewId
            }));
        });

        ajaxForm('form#interview-complete-form');

        $d.on('click', '#interview-complete-modal .btn-modal-save', function (e) {
            e.preventDefault();
            var $modal = $(this).closest('#interview-complete-modal'),
                $form = $modal.find('form');

            $form
                .attr('action', $form.attr('action').replace(/\?.*/, '') + '?' + $.param({
                    candidate_id: $modal.data('candidate-id'),
                    interview_id: $modal.data('interview-id')
                }))
                .trigger('submit');
        });
    })();

    // contact modal
    (function () {
        var $form = $('.modal-contact-form'),
            index = 0;

        $('#contact-modal').on('show.bs.modal', function (e) {
            var $this = $(this),
                candidateId = $(e.relatedTarget).closest('[data-candidate-id]').attr('data-candidate-id'),
                url = '/candidate/get-contacts?' + $.param({
                        candidate_id: candidateId
                    });

            $this.data('candidate-id', candidateId);
            // reset counter
            index = 0;
            $form.empty();

            $.post(url, $form.serialize(), 'json')
                .done(function (result, status) {
                    if ('success' !== status) {
                        return;
                    }

                    if ($.isPlainObject(result) && result.success === true
                        && $.isArray(result.data) && result.data.length
                    ) {
                        for (var i in result.data) {
                            if (!result.data.hasOwnProperty(i)) {
                                continue;
                            }
                            var contactTypeId = result.data[i].contact_type_id,
                                contactType = $('.modal-contact-menu > .si')
                                    .filter('[data-id="' + contactTypeId + '"]')
                                    .attr('class').replace(/si\-?\s*/ig, '');

                            contactType = contactType === 'email' ? 'at' : contactType;

                            $form.tmpl('tpl-contact-input', {
                                contact_type: contactType,
                                contact_type_id: contactTypeId,
                                index: index++,
                                name: result.data[i].name
                            }, true);
                        }
                    }
                });
        });

        $d
            .on('click', '.modal-contact-menu > .si', function (e) {
                e.preventDefault();
                var $this = $(this),
                    contactTypeId = +$this.data('id'),
                    contactType = $this.attr('class').replace(/si\-?\s*/ig, '');

                contactType = contactType === 'email' ? 'at' : contactType;

                $form.tmpl('tpl-contact-input', {
                    contact_type: contactType,
                    contact_type_id: contactTypeId,
                    index: index++,
                    name: ''
                }, true);
            })
            .on('click', '.modal-contact-form .btn-remove', function (e) {
                e.preventDefault();
                $(this).closest('.form-group').remove();
            })
            .on('click', '#contact-modal .btn-modal-save', function (e) {
                e.preventDefault();
                var $modal = $(this).closest('#contact-modal'),
                    url = $form.attr('action').replace(/\?.*/, '') + '?' + $.param({
                            candidate_id: $modal.data('candidate-id')
                        }),
                    $label = $('.candidate-list > li[data-candidate-id="' + $modal.data('candidate-id') + '"] .additional-info-contacts .label');

                $label.text($('.form-group', $form).length);

                $.post(url, $form.serialize(), 'json')
                    .done(function (result, status) {
                        if ('success' !== status) {
                            return;
                        }

                        if ($.isPlainObject(result) && result.success === true) {
                            $form.closest('.modal').modal('hide');
                        } else {
                            //$form.parent().html(result);
                        }
                    })
                    .fail(function () {
                        console.log('server error');
                        //$('#modal').modal('hide');
                    });
            });
    })();

    // forms
    (function () {
        $d.on('click', '#candidate-modal .btn-modal-save', function (e) {
            e.preventDefault();
        });
    })();
})(window, document, jQuery, window.doT);