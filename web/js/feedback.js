(function (window, document, undefined) {
    var $d = $(document);

    var commentRadioCheck = function (areaSelector, radioIdSelector, value) {
        var radioSelector = radioIdSelector + ' :radio[value="' + value + '"]';
        $d
            .on('click', areaSelector, function () {
                $(radioSelector).trigger('click');
            })
            .on('click', radioSelector, function () {
                $(areaSelector).trigger('focus');
            });
    };

    commentRadioCheck('#feedback-hesitation', '#feedback-met_requirements', 'not_sure');
    commentRadioCheck('#feedback-adequacy_comment', '#feedback-adequacy', 'other');
    commentRadioCheck('#feedback-potential_cooperation_comment', '#feedback-potential_cooperation', 'other');

})(this, document);
