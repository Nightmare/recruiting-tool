<?php

$path = dirname(__DIR__);

return [
    'adminEmail' => 'admin@example.com',
    'rememberFor' => 3600 * 24 * 30, // 30 days
    'webPath' => $path . '/web',
    'uploadPath' => $path . '/uploads',
    'uploadImagePath' => $path . '/web/img/uploads',
    'redmine' => [
        'url' => 'http://redmine.brightgrove.com',
        'user' => 'rest_user',
        'password' => 'r987_Use7',
    ],
    'linkedIn' => [
        'oauth2_access_token' => 'AQX6SktC71fdIxxgPclxZeats8TdbOre_SBqgFDQYS1w3DVOHBSeZq6oywBp5Vz9u6PlAV51YBhB2B15Loh94vzXMhcweQeXhpvdLxh-egZm9bLi26yDzm7x0Rgfz94rCPBr3puEWEpjGaGqh_liM0FlUu-3Mr-00PaIW9JFK_dR7AR1Gfc',
        'appId' => '75dxuxg9u7l3z1',
        'appSecret' => '5E5W8FmjfNufCgG8',
    ],
];
