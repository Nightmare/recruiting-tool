<?php

return [
    'login' => 'index/login',
    'logout' => 'index/logout',

    'POST <controller:\w+>/<id:\d+>' => '<controller>/update',
    'DELETE <controller:\w+>/<id:\d+>' => '<controller>/delete',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',

    'interview/<id:\d+>/feedback' => 'interview/feedback',
    'candidate/<type:\w+>/import' => 'candidate/import',
];