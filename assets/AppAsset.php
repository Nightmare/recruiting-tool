<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    /**
     * @inheritdoc
     */
    public $basePath = '@webroot';

    /**
     * @inheritdoc
     */
    public $baseUrl = '@web';

    /**
     * @inheritdoc
     */
    public $css = [
        '//fonts.googleapis.com/css?family=Lato:100,300,400,700,900,400italic',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
        '//jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css',
        'css/site.css',
        'css/style_1.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        '//jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js',
        'js/doT.js',
        'js/site.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\datetime\DateTimePickerAsset',
        'yii\tagsinput\TagsInputAsset',
        'kartik\typeahead\TypeaheadAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

//        \Yii::$app->getAssetManager()->bundles['yii\bootstrap\BootstrapAsset'] = [
//            'basePath' => '@webroot',
//            'baseUrl' => '@web',
//            'css' => ['css/bootstrap.css'],
//        ];
    }
}
