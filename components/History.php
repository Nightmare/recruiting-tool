<?php

namespace app\components;

use app\models\Candidate;
use Yii;
use yii\base\Object;
use app\models\History as HistoryModel;

/**
 * Class History
 * @package app\components
 */
class History extends Object
{

    public function init()
    {
        Yii::$app->on('history.create', function ($e) {
            /** @var \app\models\Candidate $candidate */
            $candidate = $e->sender;
            $this->history($candidate, HistoryModel::ACTION_CREATE);
        });

        Yii::$app->on('history.update', function ($e) {
            /** @var \app\models\Candidate $candidate */
            $candidate = $e->sender;
            $this->history($candidate, HistoryModel::ACTION_UPDATE);
        });

        Yii::$app->on('history.read', function ($e) {
            /** @var \app\models\Candidate $candidate */
            $candidate = $e->sender;
            $this->history($candidate, HistoryModel::ACTION_READ);
        });

        Yii::$app->on('history.delete', function ($e) {
            /** @var \app\models\Candidate $candidate */
            $candidate = $e->sender;
            $this->history($candidate, HistoryModel::ACTION_DELETE);
        });
    }

    /**
     * @param Candidate $candidate
     * @param string $action
     * @return bool
     */
    protected function history(Candidate $candidate, $action)
    {
        $history = new HistoryModel();
        $history->candidate_id = $candidate->id;
        $history->action = $action;

        return $history->save(false);
    }

} 