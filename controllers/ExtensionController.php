<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 25.01.15
 * Time: 20:54
 */

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UrlManager;

/**
 * Class ExtensionController
 * @package app\controllers
 */
class ExtensionController extends Controller
{

    public function actionLinkedIn()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        $headers = Yii::$app->getResponse()->getHeaders();
        $headers->add('Content-Type', 'application/xml; charset=utf-8');

        $manifest = Json::decode(file_get_contents(Yii::$app->params['webPath'] . '/extension/linkedIn_grabber/manifest.json'));
        $appId = trim(file_get_contents(Yii::$app->params['webPath'] . '/extension/linkedIn_grabber/appid'));

        return $this->renderPartial('linked-in', [
            'version' => $manifest['version'],
            'appid' => $appId,
            'codebase' => Url::to('extension/linkedIn_grabber.crx', true),
        ]);
    }

}