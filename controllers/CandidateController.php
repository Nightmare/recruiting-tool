<?php

namespace app\controllers;

use Imagine\Imagick\Imagine;
use Yii;
use app\models\Candidate;
use app\models\Contact;
use Imagine\Image\Box;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\UploadedFile;
use Happyr\LinkedIn\LinkedIn;
use app\models\CandidateSkill as Skill;

/**
 * Class CandidateController
 * @package app\controllers
 */
class CandidateController extends Controller
{

    const MAX_PHOTO_WIDTH = 558;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Candidate();

        if ($model->load(Yii::$app->getRequest()->post())) {
            $model->recruiter_id = Yii::$app->getUser()->getId();

            /** @var \app\helpers\AjaxMessage $ajaxMessage */
            $ajaxMessage = Yii::$app->ajaxMessage;

//            if (Yii::$app->getRequest()->getIsAjax()) {
//            }
            if ($model->save()) {
                $model->refresh();
                $ajaxMessage->addSuccessMessage(sprintf('%s candidate has been created', $model->getFullName()));
                Yii::$app->response->format = 'json';

                return [
                    'success' => true,
                    'messages' => $ajaxMessage->getMessages(),
                ];
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Candidate::findOne($id);
        $request = Yii::$app->getRequest();
        /** @var \app\helpers\AjaxMessage $ajaxMessage */
        $ajaxMessage = Yii::$app->ajaxMessage;
        $result = ['success' => false];

        if (!$model) {

        }

        $model->load($request->post());
        $transaction = $model->getDb()->beginTransaction();

        try {
            $model->file = UploadedFile::getInstance($model, 'logo');
            $isLogoExist = $model->file !== null;

            if ($isLogoExist) {
                $model->logo = sprintf('%s.%s', $model->file->baseName, $model->file->extension);
            }

            if ($result['success'] = $model->save()) {
                if ($isLogoExist) {
                    $path = $model->getLogoFullPath();
                    if (!FileHelper::createDirectory(dirname($path))) {
                        $ajaxMessage->addErrorMessage(sprintf('Cannot create \'%s\' file', $path));
                    }
                    $result['success'] = $model->file->saveAs($path);
                }

                if (!$result['success']) {
                    $transaction->rollBack();
                } else {
                    if ($isLogoExist) {
                        $image = new Image();
                        $size = $image->getImagine()->open($model->getLogoFullPath())->getSize();
                        if (self::MAX_PHOTO_WIDTH < $size->getWidth()) {
                            $image->thumbnail(
                                $model->getLogoFullPath(),
                                self::MAX_PHOTO_WIDTH,
                                (int)(self::MAX_PHOTO_WIDTH * $size->getHeight() / $size->getWidth())
                            )->save($model->getLogoFullPath());
                        }
                        $result['photo'] = [
                            'path' => $model->getLogoUrl(),
                        ];
                    }
                    $transaction->commit();
                }
            } else {
                $transaction->rollBack();
                $ajaxMessage->addModelErrors($model);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $ajaxMessage->addExceptionError($e);
//                throw $e;
        }

        if ($result['success']) {
            $ajaxMessage->addSuccessMessage(sprintf('%s\'s profile has been updated', $model->getFullName()));
        } else {
            $ajaxMessage->addErrorMessage(sprintf('Cannot update %s\'s profile', $model->getFullName()));
        }

        $result['messages'] = $ajaxMessage->getMessages();

        if ($request->getIsAjax()) {
            $model->refresh();
            Yii::$app->response->format = 'json';
            return $result;
        }
    }

    public function actionDelete($id)
    {
        $model = Candidate::findOne($id);
        $request = Yii::$app->getRequest();
        /** @var \app\helpers\AjaxMessage $ajaxMessage */
        $ajaxMessage = Yii::$app->ajaxMessage;
        $result = ['success' => false];

        if (!$model) {

        } else {
            $result['success'] = $model::findOne($id)->delete();
        }


        if ($request->getIsAjax()) {
            $model->refresh();
            Yii::$app->response->format = 'json';
            return $result;
        }
    }

    public function actionSetCroppedPhoto()
    {
        $request = Yii::$app->getRequest();
        $size = $request->get('dimensions');
        $model = Candidate::findOne($request->get('candidate_id'));

        /** @var \app\helpers\AjaxMessage $ajaxMessage */
        $ajaxMessage = Yii::$app->ajaxMessage;

        if (!$size || !$model) {

        }

        $result = [
            'success' => true,
        ];

        $transaction = $model->getDb()->beginTransaction();

        try {
            $fileName = pathinfo($model->logo, PATHINFO_FILENAME);
            $extension = pathinfo($model->logo, PATHINFO_EXTENSION);

            $image = Image::crop($model->getLogoFullPath(), $size['w'], $size['h'], [$size['x'], $size['y']]);

            $model->logo = $fileName . '-110x110.' . $extension;

            if ($model->save()) {
                $image
                    ->thumbnail(new Box(110, 110))
                    ->save($model->getLogoFullPath());

                $ajaxMessage->addSuccessMessage(sprintf('%s\'s avatar has been cropped', $model->getFullName()));

                $result['path'] = $model->getLogoUrl();
                $transaction->commit();
            } else {
                $ajaxMessage->addErrorMessage(sprintf('Cannot crop %s\'s avatar', $model->getFullName()));

                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $ajaxMessage->addExceptionError($e);
            $transaction->rollBack();

            $result['success'] = false;
        }

        $result['messages'] = $ajaxMessage->getMessages();

        if ($request->getIsAjax()) {
            $model->refresh();
            Yii::$app->response->format = 'json';
            return $result;
        }
    }

    public function actionSaveContacts()
    {
        $request = Yii::$app->getRequest();
        $candidateId = $request->get('candidate_id');
        $model = Candidate::findOne($candidateId);
        $contacts = $request->post('Contact');

        Contact::deleteAll(['candidate_id' => $candidateId]);

        foreach ($contacts as $contact) {
            if (empty($contact['name'])) {
                continue;
            }

            $contact['candidate_id'] = $candidateId;
            $contactModel = new Contact();
            $contactModel->setAttributes($contact);
            $contactModel->save();
        }

        /** @var \app\helpers\AjaxMessage $ajaxMessage */
        $ajaxMessage = Yii::$app->ajaxMessage;
        $ajaxMessage->addSuccessMessage(sprintf('%s\'s contacts has been updated', $model->getFullName()));

        $result = [
            'success' => true,
            'messages' => $ajaxMessage->getMessages(),
        ];

        if ($request->getIsAjax()) {
            Yii::$app->response->format = 'json';
            return $result;
        }
    }

    public function actionGetContacts()
    {
        $request = Yii::$app->getRequest();
        $candidateId = $request->get('candidate_id');
        $model = Candidate::findOne($candidateId);

        $contacts = $model->getContacts()->all();

        $result = [
            'success' => true,
            'data' => $contacts,
        ];

        if ($request->getIsAjax()) {
            Yii::$app->response->format = 'json';
            return $result;
        }
    }

    public function actionImport($type)
    {
        $request = Yii::$app->getRequest();
        $success = true;
        $userMessageCode = 'error';

        switch ($type) {
            case 'linkedin':
                $user = $this->linkedInImport($request->get('publicUrl'));
                $identityId = join(':', [$type, $request->get('linkedinId', '')]);

                if (Candidate::findOne(['identity_id' => $identityId])) {
                    $userMessageCode = 'exist';
                    break;
                }

                // skills
                if (!empty($user['skills']['values'])) {
                    $skills = array_map(function ($skillValue) {
                        return $skillValue['skill']['name'];
                    }, $user['skills']['values']);
                } else {
                    $skills = $request->get('skills', []);
                }

                // full name
                if (isset($user['firstName'], $user['lastName'])) {
                    $firstName = $user['firstName'];
                    $lastName = $user['lastName'];
                } else {
                    list($firstName, $lastName) = explode(' ', preg_replace('/\s+(.*)\s+/', ' ', $request->get('fullName', ' ')));
                }

                // current position
                if (!empty($user['positions']['values'])) {
                    $currentPosition = null;
                    foreach ($user['positions']['values'] as $i => $position) {
                        if ($i === 0) {
                            $currentPosition = $position['title'];
                        }

                        if (!empty($position['isCurrent'])) {
                            $currentPosition = $position['title'];
                            break;
                        }
                    }
                } else {
                    $currentPosition = $request->get('currentPosition', null);
                }

                // picture url
                $pictureUrl = $request->get('pictureUrl', '');
                if (!$pictureUrl && !empty($user['pictureUrl'])) {
                    $pictureUrl = $user['pictureUrl'];
                }

                // duration
                $now = new \DateTime();
                $interval = clone $now;
                foreach ($request->get('workPeriods', []) as $workPeriod) {
                    try {
                        $interval->modify('+ ' . $workPeriod);
                    } catch (\Exception $e) {

                    }
                }

                $experience = $interval->diff($now)->y;

                $candidate = new Candidate();
                $candidate->recruiter_id = Yii::$app->getUser()->getId();//todo: change to behaviour
                $candidate->first_name = $firstName;
                $candidate->last_name = $lastName;
                $candidate->position = $currentPosition;
                $candidate->identity_id = $identityId;
                $candidate->experience = $experience;

                if ($success = $candidate->save(false)) {
                    $userMessageCode = 'success';

                    if (!empty($pictureUrl)) {
                        $candidate->logo = $identityId;
                        $candidate->save(false);
                        $path = $candidate->getLogoFullPath();

                        if (!FileHelper::createDirectory(dirname($path))) {
// todo error
                        } else {
                            $image = (new Imagine())->read(fopen($pictureUrl, 'rb'));
                            $image->thumbnail(new Box(110, 110))
                                ->save($path);
                        }
                    }

                    foreach ($skills as $skillName) {
                        $skill = new Skill();
                        $skill->candidate_id = $candidate->id;
                        $skill->skill = trim($skillName, '.');

                        $skill->save(false);
                    }
                }

                break;
        }

        Yii::$app->response->format = 'json';
        return [
            'success' => $success,
            'userMessageCode' => $userMessageCode,
        ];
    }

    /**
     * @param $publicUrl
     * @return array|string
     */
    protected function linkedInImport($publicUrl)
    {
        $params = Yii::$app->params;
        $linkedin = new LinkedIn($params['linkedIn']['appId'], $params['linkedIn']['appSecret']);

        try {
            $user = $linkedin->api(
                '/v1/people/url=' . urlencode($publicUrl) . ':(id,first-name,last-name,positions,picture-url,email-address,skills,phone-numbers,twitter-accounts)',
                ['oauth2_access_token' => $params['linkedIn']['oauth2_access_token']]
            );
            return $user;
        } catch (\Exception $e) {
            return [];
        }


    }
}
