<?php

namespace app\controllers;

use app\models\CandidateSkill as Skill;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\ContentNegotiator;

/**
 * Class SkillController
 * @package app\controllers
 */
class SkillController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionRead()
    {
        $request = \Yii::$app->getRequest();

        $candidateId = $request->get('candidate_id');
        $skills = [];

        if ($candidateId) {
            $skills = array_map(function ($skillsModel) {
                return $skillsModel['skill'];
            }, Skill::find()->where(['candidate_id' => $candidateId])->asArray()->all());
        }

        if ($request->getIsAjax()) {
            return [
                'success' => true,
                'data' => $skills,
            ];
        }
    }

    public function actionUpdate()
    {
        $request = \Yii::$app->getRequest();

        $skills = $request->get('skills', []);
        $candidateId = $request->get('candidate_id');

        if ($candidateId) {
            Skill::deleteAll(['candidate_id' => $candidateId]);
            foreach ($skills as $skillName) {
                $skill = new Skill();
                $skill->setAttributes([
                    'candidate_id' => $candidateId,
                    'skill' => trim($skillName),
                ]);

                $skill->save();
            }
        }

        if ($request->getIsAjax()) {
            return [
                'success' => true,
            ];
        }
    }

} 