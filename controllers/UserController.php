<?php

namespace app\controllers;

use app\models\Candidate;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;


/**
 * Class UserController
 * @package app\controllers
 */
class UserController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        /** @var User[] $users */
        $request = \Yii::$app->getRequest();

        if (null !== ($term = $request->get('term'))) {
            $users = User::find()->andWhere(['like', 'full_name', $term])->all();
        } else {
            $users = User::find()->all();
        }

        if (\Yii::$app->getRequest()->getIsAjax()) {
            \Yii::$app->response->format = 'json';

            return [
                'success' => true,
                'data' => $users,
            ];
        }

        return $this->render($this->action->id, [
            'models' => $users,
        ]);
    }

}
