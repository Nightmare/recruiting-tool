<?php

namespace app\controllers;

use app\models\Cv;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\filters\ContentNegotiator;
use yii\web\UploadedFile;

/**
 * Class CvController
 * @package app\controllers
 */
class CvController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $request = \Yii::$app->getRequest();
        if ($request->getIsPost()) {
            $model = new Cv();
            $model->load($request->post());
            $model->file = UploadedFile::getInstance($model, 'path');
            $success = false;

            /** @var \app\helpers\AjaxMessage $ajaxMessage */
            $ajaxMessage = \Yii::$app->ajaxMessage;

            $model->path = sprintf('%s.%s', $model->file->baseName, $model->file->extension);

            $transaction = $model->getDb()->beginTransaction();
            try {
                if ($model->save(false)) {
                    $path = $model->getFullPath();
                    FileHelper::createDirectory(dirname($path));
                    $success = $model->file->saveAs($path);

                    if (!$success) {
                        $transaction->rollBack();
                    } else {
                        $transaction->commit();
                    }
                } else {
                    $transaction->rollBack();
                    $ajaxMessage->addModelErrors($model);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $ajaxMessage->addExceptionError($e);
//                throw $e;
            }

            if ($request->getIsAjax()) {
                $model->refresh();
                return [
                    'success' => $success,
                    'data' => $model,
                ];
            }
        }

    }

    public function actionRead()
    {
        $request = \Yii::$app->getRequest();
        $candidateId = $request->get('candidate_id');

        if ($candidateId && $request->getIsAjax()) {
            return [
                'success' => true,
                'data' => Cv::findAll(['candidate_id' => $candidateId]),
            ];
        }
    }

    public function actionDelete()
    {
        $request = \Yii::$app->getRequest();
        $success = false;

        if ($id = $request->get('id')) {
            $success = Cv::findOne($id)->delete();
        }

        if ($request->getIsAjax()) {

            /** @var \app\helpers\AjaxMessage $ajaxMessage */
            $ajaxMessage = \Yii::$app->ajaxMessage;

            if ($success = (bool)$success) {
                $ajaxMessage->addSuccessMessage('CV has been deleted');
            } else {
                $ajaxMessage->addSuccessMessage('Cannot delete CV');
            }

            return [
                'success' => (bool)$success,
                'messages' => $ajaxMessage->getMessages(),
            ];
        }
    }


    public function actionDownload()
    {
        if (($id = \Yii::$app->getRequest()->get('id')) && ($cvModel = Cv::findOne($id))) {
            return \Yii::$app->getResponse()
                ->sendContentAsFile(
                    file_get_contents($cvModel->getFullPath()),
                    basename($cvModel->path)
                );
        }

        throw new HttpException(404, 'The CV could not be found.');
    }

} 