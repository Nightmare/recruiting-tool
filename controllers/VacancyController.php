<?php

namespace app\controllers;

use app\models\Vacancy;
use Yii;
use app\models\Candidate;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class VacancyController
 * @package app\controllers
 */
class VacancyController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $request = Yii::$app->getRequest();

        /** @var Yii\db\ActiveQuery $accounts */
        $accounts = Vacancy::find();

        if (null !== ($term = $request->get('query'))) {
            $accounts->andWhere(['like', 'name', $term]);
        }

        $accounts = $accounts->all();

        if (Yii::$app->getRequest()->getIsAjax()) {
            Yii::$app->response->format = 'json';

            return [
                'success' => true,
                'data' => $accounts,
            ];
        }

        return $this->render($this->action->id, [
            'models' => $accounts,
        ]);
    }

}
