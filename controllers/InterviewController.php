<?php

namespace app\controllers;

use app\forms\interview\Complete as InterviewComplete;
use app\forms\SendNotification;
use Yii;
use app\models\InterviewParticipant;
use app\forms\Interview;
use app\models\Candidate;
use app\models\Feedback;
use app\models\User;
use Eluceo\iCal\Component\Calendar as iCalCalendar;
use Eluceo\iCal\Component\Event as iCalEvent;
use yii\base\Event;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Interview as InterviewModel;

/**
 * Class InterviewController
 * @package app\controllers
 */
class InterviewController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['feedback'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $interviewForm = new Interview();

        if ($interviewId = (int)$request->get('interview_id')) {
            $interviewModel = InterviewModel::findOne($interviewId);
        } else {
            $interviewModel = new InterviewModel();
        }

        if ($request->getIsPost() && $interviewForm->load($request->post())) {
            /** @var \app\helpers\AjaxMessage $ajaxMessage */
            $ajaxMessage = Yii::$app->ajaxMessage;

            if ($interviewForm->validate()) {
                if ($interviewModel->getIsNewRecord() && $interviewForm->interview_id) {
                    $interviewModel = InterviewModel::findOne($interviewForm->interview_id);
                }
                $interviewModel->setAttributes($interviewForm->getAttributes());

                $transaction = Yii::$app->getDb()->beginTransaction();

                try {
                    if (false === $interviewModel->save(false)) {
                        throw new \Exception('Cannot update interview');
                    }

                    $candidate = Candidate::findOne($interviewForm->candidate_id);

                    // remove all related
                    InterviewParticipant::deleteAll(['interview_id' => $interviewModel->id]);

                    $participants = explode(',', $interviewForm->participants);
                    $users = User::find()->where(['full_name' => $participants])->all();

                    // save into history
//                    Yii::$app->trigger('history.create', new Event(['sender' => $candidate, 'data' => ['asd', '123']]));

                    /** @var User $userModel */
                    foreach ($users as $userModel) {
                        $interviewParticipant = new InterviewParticipant();

                        $interviewParticipant->user_id = $userModel->id;
                        $interviewParticipant->interview_id = $interviewModel->id;
                        $interviewParticipant->candidate_id = $candidate->id;

                        if (false === $interviewParticipant->save()) {
                            throw new \Exception('Cannot update interview participant');
                        }
                    }

                    if ($interviewForm->send_invitation) {
                        $subject = [$candidate->getFullName(), $interviewForm->vacancy, $interviewForm->account];
                        $subject = Yii::t('app', 'Interview') . ': ' . join(' > ', $subject);

                        /** @var User $currentUser */
                        $currentUser = Yii::$app->getUser()->getIdentity();

                        $mailer = Yii::$app->mailer
                            ->compose('interview/create', [
                                'comment' => $interviewForm->comment,
                                'isAttachCv' => $interviewForm->attach_cv,
                                'appointmentAt' => $interviewForm->appointment_at,
                            ])
                            ->setFrom([$currentUser->email => $currentUser->full_name])
                            ->setSubject($subject);

                        if ($interviewForm->attach_cv) {
                            /** @var \app\models\Cv $cv */
                            foreach (Candidate::findOne($interviewForm->candidate_id)->getCvs()->all() as $cv) {
                                $mailer->attach($cv->getFullPath());
                            }
                        }

                        //region iCal reminder
                        $utcTz = new \DateTimeZone('UTC');
                        $dateInterval = new \DateInterval('PT1H');

                        $appointmentStart = new \DateTime($interviewForm->appointment_at);
                        $appointmentStart->setTimezone($utcTz);

                        $appointmentEnd = new \DateTime();
                        $appointmentEnd->setTimestamp($appointmentStart->getTimestamp());
                        $appointmentEnd->setTimezone($utcTz);
                        $appointmentEnd->add($dateInterval);

                        $vEvent = (new iCalEvent(sha1($interviewModel->id) . '@recruiting.brightgrove.com'))
                            ->setDtStart($appointmentStart)
                            ->setDtEnd($appointmentEnd)
                            ->setDuration($dateInterval)
                            ->setLocation('Kharkiv, Brightgrove Ltd, Kooperativnaya str, 1a, Meeting room')
                            ->setStatus(iCalEvent::STATUS_CONFIRMED)
                            ->setSequence(0)
                            ->setUseUtc(true)
                            ->setOrganizer("CN={$currentUser->full_name}:mailto:{$currentUser->email}")
                            ->setSummary($subject);

                        if ($interviewForm->comment) {
                            $vEvent->setDescription($interviewForm->comment);
                        }

                        $vCalendar = new iCalCalendar('Recruiting Reminders');
                        $vCalendar->addComponent($vEvent);
                        $vCalendar->setMethod(iCalCalendar::METHOD_REQUEST);
                        $iCalContent = $vCalendar->render();

                        $attendee = '';
                        $usersMail = [];
                        /** @var User $user */
                        foreach ($users as $user) {
                            $attendee .= sprintf(
                                '%sATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=%s;X-NUM-GUESTS=0:mailto:%s',
                                "\r\n",
                                $user->full_name,
                                $user->email
                            );
                            $usersMail[$user->email] = $user->full_name;
                        }
                        $mailer->setTo($usersMail);
                        unset($usersMail);

                        $iCalContent = preg_replace('/\s+END:VEVENT/', $attendee . '$0', $iCalContent);
                        unset($attendee);

                        $mailer->attachContent($iCalContent, [
                            'fileName' => 'invite.ics',
                            'contentType' => 'text/calendar; charset=utf-8; method=REQUEST',
                        ]);

                        $mailer->send();
                    }

                    $ajaxMessage->addSuccessMessage(sprintf('Interview for %s has been created', $candidate->getFullName()));
                    Yii::$app->response->format = 'json';

                    $transaction->commit();

                    return [
                        'success' => true,
                        'messages' => $ajaxMessage->getMessages(),
                    ];
                } catch (\Exception $e) {
                    $ajaxMessage->addExceptionError($e);
                    $transaction->rollBack();
                }
            }
        } elseif ($interviewId) {
            $interviewForm->setAttributes($interviewModel->getAttributes());
            /** @var InterviewParticipant $interviewParticipant */
            foreach ($interviewModel->getInterviewParticipants()->all() as $interviewParticipant) {
                $interviewForm->candidate_id = $interviewParticipant->candidate_id;
                $interviewForm->participants .= $interviewParticipant->getUser()->one()->getFullName() . ',';
            }
            $interviewForm->participants = rtrim($interviewForm->participants, ',');
            $interviewForm->vacancy = $interviewModel->getVacancy()->one()->name;
            $interviewForm->account = $interviewModel->getAccount()->one()->name;
            $interviewForm->interview_id = $interviewModel->id;

        } elseif ($candidateId = (int)$request->get('candidate_id')) {
            $interviewForm->candidate_id = $candidateId;
            $interviewForm->participants = $this->getRequiredParticipants($candidateId);
        }

        return $this->renderAjax($this->action->id, [
            'model' => $interviewForm,
        ]);
    }

    public function actionFeedback($id)
    {
        $model = new Feedback();
        $request = Yii::$app->getRequest();

        if ($model->load($request->post())) {
            $model->interview_id = $id;

            if ($model->save()) {
                return $this->render('feedback/success');
            }
        }

        return $this->render($this->action->id, [
            'model' => $model,
        ]);
    }

//    public function actionInterviewComplete()
//    {
//        $request = Yii::$app->getRequest();
//        $form = new InterviewComplete();
//
//        if ($form->load($request->post())) {
//            /** @var \app\helpers\AjaxMessage $ajaxMessage */
//            $ajaxMessage = Yii::$app->ajaxMessage;
//
//            if ($form->validate()) {
//                /** @var User $user */
//                $user = Yii::$app->getUser()->getIdentity();
//                $candidate = Candidate::findOne($form->candidate_id);
//                $participants = User::find()->where(['full_name' => explode(',', $form->participants)])->all();
//                $interview = InterviewModel::findOne($form->interview_id);
//
//                $transaction = Yii::$app->getDb()->beginTransaction();
//
//                try {
//                    $interview->status = InterviewModel::STATUS_COMPLETED;
//                    $interview->save(false);
//
//                    $url = Url::to(['interview/feedback', 'id' => $form->interview_id], true);
//
//                    $mailer = Yii::$app->mailer
//                        ->compose('interview/interview-complete', [
//                            'content' => str_replace(
//                                '#link',
//                                Html::a($url, $url),
//                                $form->content
//                            ),
//                        ])
//                        ->setFrom([$user->email => $user->full_name])
//                        ->setSubject('Recruiting system. Feedback link for ' . $candidate->getFullName());
//
//                    $participantsMail = [];
//                    /** @var User $participant */
//                    foreach ($participants as $participant) {
//                        $participantsMail[$participant->email] = $participant->full_name;
//                    }
//                    $mailer->setTo($participantsMail);
//                    unset($participantsMail);
//
//                    if ($mailer->send()) {
//                        $transaction->commit();
//                    } else {
//                        $transaction->rollBack();
//                    }
//                } catch (\Exception $e) {
//                    $ajaxMessage->addExceptionError($e);
//                    $transaction->rollBack();
//                }
//            }
//        } elseif (($interviewId = (int)$request->get('interview_id')) && ($candidateId = (int)$request->get('candidate_id'))) {
//            $interview = InterviewModel::findOne($interviewId);
//            $candidate = Candidate::findOne($candidateId);
//            $form->interview_id = $interviewId;
//            $form->content = ['Hello,',
//                'Please click here: #link',
//                'to provide feedback after interviewing ' . $candidate->getFullName() . '.'];
//
//            $form->content = join(PHP_EOL, $form->content);
//
//            /** @var InterviewParticipant $interviewParticipant */
//            foreach ($interview->getInterviewParticipants()->all() as $interviewParticipant) {
//                $form->candidate_id = $interviewParticipant->candidate_id;
//                $form->participants .= $interviewParticipant->getUser()->one()->getFullName() . ',';
//            }
//            $form->participants = rtrim($form->participants, ',');
//        }
//
//        return $this->renderAjax($this->action->id, [
//            'model' => $form,
//        ]);
//    }

    /**
     * @return string
     */
    public function actionSendNotification()
    {
        $request = Yii::$app->getRequest();
        $form = new SendNotification();

        if ($form->load($request->post())) {
            /** @var \app\helpers\AjaxMessage $ajaxMessage */
            $ajaxMessage = Yii::$app->ajaxMessage;

            if ($form->validate()) {
                /** @var User $user */
                $user = Yii::$app->getUser()->getIdentity();
                $candidate = Candidate::findOne($form->candidate_id);
                $participants = User::find()->where(['full_name' => explode(',', $form->participants)])->all();

                $mailer = Yii::$app->mailer
                    ->compose('interview/send-notification', [
                        'comment' => $form->comment,
                    ])
                    ->setFrom([$user->email => $user->full_name])
                    ->setSubject('Notification mail for interview for ' . $candidate->getFullName());

                $participantsMail = [];
                /** @var User $participant */
                foreach ($participants as $participant) {
                    $participantsMail[$participant->email] = $participant->full_name;
                }
                $mailer->setTo($participantsMail);
                unset($participantsMail);

                if ($form->attach_cv) {
                    /** @var \app\models\Cv $cv */
                    foreach ($candidate->getCvs()->all() as $cv) {
                        $mailer->attach($cv->getFullPath());
                    }
                }

                if ($mailer->send()) {
                    $ajaxMessage->addSuccessMessage('Notification successfully has been sent');
                } else {
                    $ajaxMessage->addErrorMessage('Cannot send notification');
                }

                Yii::$app->response->format = 'json';
                return [
                    'success' => true,
                    'messages' => $ajaxMessage->getMessages(),
                ];
            }
        } elseif ($candidateId = (int)$request->get('candidate_id')) {
            $form->candidate_id = $candidateId;
            $form->participants = $this->getRequiredParticipants($candidateId);
        }

        return $this->renderAjax($this->action->id, [
            'model' => $form,
        ]);
    }

    /**
     * @param int $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        InterviewModel::findOne($id)->delete();
        return $this->goBack();
    }

    /**
     * @param int $candidateId
     * @return string
     */
    protected function getRequiredParticipants($candidateId)
    {
        return join(',', array_unique([
            Yii::$app->getUser()->getIdentity()->getFullName(),
            Candidate::findOne($candidateId)->getRecruiter()->one()->getFullName()
        ]));
    }
}
