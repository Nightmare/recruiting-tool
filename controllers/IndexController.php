<?php

namespace app\controllers;

use app\models\Candidate;
use app\models\Interview as InterviewModel;
use app\forms\Interview as InterviewForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\forms\Login as LoginForm;

/**
 * Class IndexController
 * @package app\controllers
 */
class IndexController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
//        $redmineClient = new RedmineClient(Yii::$app->params['redmineUrl'], 'b0470207d8294787c0532455bad910a2a8608162');
//
//        /** @var \Redmine\Api\User $redmineUser */
//        $redmineUser = $redmineClient->api('user');
//        $users = $redmineUser->all();

        $candidates = new ActiveDataProvider([
            'query' => Candidate::find()->orderBy(['created_at' => SORT_DESC]),
            'pagination' => ['pageSize' => 4],
        ]);

        return $this->render('index', [
            'candidates' => $candidates,
            'candidateModel' => new Candidate(),
            'interviewForm' => new InterviewForm(),
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->getUser()->getIsGuest()) {
            return $this->goHome();
        }

        $this->layout = false;

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
