<?php

namespace app\commands;

use app\models\Account;
use app\models\Vacancy;
use yii\console\Controller;
use yii\db\Connection;

/**
 * Class VacancyController
 * @package app\commands
 */
class VacancyController extends Controller
{

    public function actionIndex()
    {
        $connection = new Connection([
            'dsn' => 'mysql:host=crm.brightgrove.com;dbname=sugarcrm',
            'username' => 'recrut',
            'password' => 'REpx39mX',
            'charset' => 'utf8',
        ]);

        $vacancies = $connection->createCommand(
            'SELECT c.id redmine_vacancy_id, c.name, c.account_id, IF (c.deleted = 0, NULL, NOW()) `deleted_at`,
            c.description, c.`date_entered` `created_at`, c.`date_modified` `updated_at`, c.status,
            IF (c.priority = "P1", "high", IF (c.priority = "P2", "normal", "low")) importance,
            d.date_opened_c opened_at, d.date_start_c startet_at, d.date_closed_c closed_at, d.location_c location
            FROM cases c
            JOIN cases_cstm d ON d.id_c = c.id
            JOIN accounts a ON c.account_id = a.id'
        )->queryAll();

        $columns = Vacancy::getTableSchema()->getColumnNames();
        unset($columns[array_search('id', $columns)]);
        $columns = array_values($columns);

        foreach ($vacancies as $_vacancy) {
            if (!($account = Account::findOne(['redmine_account_id' => $_vacancy['account_id']]))) {
                continue;
            }

            if (!($vacancy = Vacancy::findOne(['redmine_vacancy_id' => $_vacancy['redmine_vacancy_id']]))) {
                $vacancy = new Vacancy();
            }

            $vacancy->setAttributes(array_intersect_key($_vacancy, array_flip($columns)));
            $vacancy->account_id = $account->id;

            $vacancy->save(false);
        }
    }
}
