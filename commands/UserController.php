<?php

namespace app\commands;

use app\models\User;
use Redmine\Client;
use yii\console\Controller;

/**
 * Class UserController
 * @package app\commands
 */
class UserController extends Controller
{

    public function actionIndex()
    {
        $redmineParams = \Yii::$app->params['redmine'];
        $client = new Client($redmineParams['url'], $redmineParams['user'], $redmineParams['password']);

        /** @var \Redmine\Api\User $users */
        $users = $client->api('user');
        $users = $users->all(['limit'  => 500]);

        if (200 !== $client->getResponseCode() || empty($users['users']) || !is_array($users['users'])) {
            return null;
        }

        foreach ($users['users'] as $redmineUser) {
            $user = User::findOne(['redmine_id' => $redmineUser['id']]);
            if (!$user) {
                $user = new User();
            }

            $user->setAttributes([
                'redmine_id' => $redmineUser['id'],
                'email' => $redmineUser['mail'],
                'first_name' => $redmineUser['firstname'],
                'last_name' => $redmineUser['lastname'],
                'login' => $redmineUser['login'],
                'created_at' => (new \DateTime($redmineUser['created_on']))->format('Y-m-d H:i:s'),
            ]);

            $user->save(false);
        }
    }
}
