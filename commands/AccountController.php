<?php

namespace app\commands;

use app\models\Account;
use yii\console\Controller;
use yii\db\Connection;

/**
 * Class AccountController
 * @package app\commands
 */
class AccountController extends Controller
{

    public function actionIndex()
    {
        $connection = new Connection([
            'dsn' => 'mysql:host=crm.brightgrove.com;dbname=sugarcrm',
            'username' => 'recrut',
            'password' => 'REpx39mX',
            'charset' => 'utf8',
        ]);

        $accounts = $connection->createCommand(
            'SELECT *, id redmine_account_id, `account_type` `type`, IF (`website` = "http://", NULL, `website`) `website`,
                    IF (`deleted` = 0, NULL, NOW()) `deleted_at`, `date_entered` `created_at`, `date_modified` `updated_at`
              FROM `accounts`'
        )->queryAll();

        $columns = Account::getTableSchema()->getColumnNames();
        unset($columns[array_search('id', $columns)]);
        $columns = array_values($columns);

        foreach ($accounts as $_account) {
            if (!($account = Account::findOne(['redmine_account_id' => $_account['redmine_account_id']]))) {
                $account = new Account();
            }

            $account->setAttributes(array_intersect_key($_account, array_flip($columns)));
            $account->save(false);
        }
    }
}
