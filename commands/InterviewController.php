<?php

namespace app\commands;

use Yii;
use app\models\Interview;
use app\models\Candidate;
use app\models\InterviewParticipant;
use app\models\User;
use yii\console\Controller;
use yii\helpers\Html;

/**
 * Class InterviewController
 * @package app\commands
 */
class InterviewController extends Controller
{

    public function actionCheckCompleted()
    {
        $dateTime = new \DateTime('now + 30minutes');
        /** @var Interview $interviews */
        $interviews = Interview::find()
            ->where('status <> :status AND appointment_at <= :date', [
                ':date' => $dateTime->format('Y-m-d H:i:s'),
                ':status' => Interview::STATUS_COMPLETED,
            ])
            ->all();

        foreach ($interviews as $interview) {
            $this->sendFeedbackNotification($interview);
        }
    }

    /**
     * @param Interview $interview
     * @throws \yii\db\Exception
     */
    protected function sendFeedbackNotification(Interview $interview)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            /** @var User $user */
            $user = $interview->getUser()->one();

            $interviewParticipants = $interview->getInterviewParticipants();
            $participants = [];

            /** @var InterviewParticipant $interviewParticipant */
            foreach ($interviewParticipants->all() as $interviewParticipant) {
                /** @var User $_user */
                $_user = $interviewParticipant->getUser()->one();
                $participants[$_user->email] = $_user->getFullName();
                /** @var Candidate $candidate */
                $candidate = $interviewParticipant->getCandidate()->one();
            }

            if (!$participants || !isset($candidate)) {
                return;
            }

            $url = Yii::$app->getUrlManager()->createAbsoluteUrl(['interview/feedback', 'id' => $interview->id], true);

            $mailer = Yii::$app->getMailer()
                ->compose('interview/interview-complete', [
                    'content' => str_replace(
                        '#link',
                        Html::a($url, $url),
                        join(PHP_EOL, [
                            'Hello,',
                            'Please click here: #link',
                            'to provide feedback after interviewing ' . $candidate->getFullName() . '.'
                        ])
                    ),
                ])
                ->setFrom([$user->email => $user->full_name])
                ->setSubject('Recruiting system. Feedback link for ' . $candidate->getFullName());

            $mailer->setTo($participants);

            $interview->status = Interview::STATUS_COMPLETED;
            $interview->save(false, ['status']);

            if ($mailer->send()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
        }
    }
}
