<?php

namespace app\models;

use Yii;
use app\models\base\CvBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "cv".
 */
class Cv extends CvBase
{

    /**
     * Virtual field for file uploading
     *
     * @var string|null
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['file', 'safe'],
            ['file', 'file'],
        ]);


    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_DELETE => 'deleted_at',
                    self::EVENT_BEFORE_VALIDATE => 'created_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return implode(DIRECTORY_SEPARATOR, [Yii::$app->params['uploadPath'], $this->candidate_id, 'cv', $this->path]);
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $path = $this->getFullPath();
        if ($result = parent::delete()) {
            @unlink($path);
        }

        return $result;
    }

}
