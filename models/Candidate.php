<?php

namespace app\models;

use Yii;
use app\models\base\Candidate as CandidateBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "candidate".
 */
class Candidate extends CandidateBase
{

    const SKILLS_TYPE_ROCKSTAR = 'rockstar';
    const SKILLS_TYPE_JACKPOT = 'jackpot';

    /**
     * Virtual field for file uploading
     *
     * @var string|null
     */
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['file', 'safe'],
            ['file', 'file', 'extensions' => 'jpg, gif, png', 'maxSize' => 12 * 1024 * 1024, 'minSize' => 5 * 1024], // 12MB and 5Kb
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                    self::EVENT_BEFORE_DELETE => 'deleted_at',

                    self::EVENT_BEFORE_VALIDATE => 'created_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return implode(' ', [$this->first_name, $this->last_name]);
    }

    /**
     * @return bool Whether user is deleted.
     */
    public function getIsDeleted()
    {
        return null === $this->deleted_at;
    }

    /**
     * @return string
     */
    public function getLogoFullPath()
    {
        return implode(DIRECTORY_SEPARATOR, [$this->getLogoDirName(), $this->logo]);
    }

    /**
     * @return string
     */
    public function getLogoUrl()
    {
        return implode(DIRECTORY_SEPARATOR, [$this->getLogoDirUrl(), $this->logo]);
    }

    /**
     * @return string
     */
    public function getLogoDirUrl()
    {
        return implode('/', [rtrim(\Yii::$app->getHomeUrl(), '\/'), 'img', 'uploads', 'candidate', $this->id, 'logo']);
    }

    /**
     * @return string
     */
    public function getLogoDirName()
    {
        return implode(DIRECTORY_SEPARATOR, [\Yii::$app->params['uploadImagePath'], 'candidate', $this->id, 'logo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviews()
    {
        return $this->hasMany(\app\models\Interview::className(), ['id' => 'interview_id'])->viaTable('interview_participant', ['candidate_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $path = $this->getLogoFullPath();
        if ($result = parent::delete()) {
            @unlink($path);
        }

        return $result;
    }

}
