<?php

namespace app\models;

use Yii;
use app\models\base\Interview as InterviewBase;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "interview".
 */
class Interview extends InterviewBase
{

    const STATUS_COMPLETED = 'completed';
    const STATUS_UNDUE = 'undue';
    const STATUS_IN_DOUBT = 'in_doubt';
    const STATUS_ACCEPTED = 'accepted';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                'comment',
                'match', 'pattern' => '/^[a-zA-z0-9\s\.\-_\\\/,:;]$/',
                'message' => 'Comment should be written in english only.',
            ],
            ['appointment_at', 'date', 'format' => 'php:Y-m-d H:i'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                    self::EVENT_BEFORE_DELETE => 'deleted_at',
                    self::EVENT_BEFORE_VALIDATE => 'created_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'status',
                ],
                'value' => self::STATUS_UNDUE,
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'comment' => Yii::t('feedback', 'Comment (English only)'),
        ]);
    }

}
