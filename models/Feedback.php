<?php

namespace app\models;

use Yii;
use app\models\base\Feedback as FeedbackBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "feedback".
 */
class Feedback extends FeedbackBase
{

    const LEVEL_JUNIOR = 'J';
    const LEVEL_JUNIOR_UPPER = 'JU';
    const LEVEL_MIDDLE = 'M';
    const LEVEL_MIDDLE_UPPER = 'MU';
    const LEVEL_SENIOR = 'S';
    const LEVEL_SENIOR_UPPER = 'SU';

    const REQUIREMENTS_YES = 'yes';
    const REQUIREMENTS_NO = 'no';
    const REQUIREMENTS_NOT_SURE = 'not_sure';

    const ADEQUACY_YES = 'yes';
    const ADEQUACY_NO = 'no';
    const ADEQUACY_NO_FULLY = 'not_fully';
    const ADEQUACY_NOT_SURE = 'other';

    const POTENTIAL_COOPERATION_YES = 'yes';
    const POTENTIAL_COOPERATION_NO = 'no';
    const POTENTIAL_COOPERATION_OTHER = 'other';

    const ENGLISH_LEVEL_BEGINNER = 'beginner';
    const ENGLISH_LEVEL_ELEMENTARY = 'elementary';
    const ENGLISH_LEVEL_PRE_INTERMEDIATE = 'pre-intermediate';
    const ENGLISH_LEVEL_INTERMEDIATE = 'intermediate';
    const ENGLISH_LEVEL_UPPER_INTERMEDIATE = 'upper-intermediate';
    const ENGLISH_LEVEL_ADVANCED = 'advanced';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['hesitation', 'adequacy_comment', 'potential_cooperation_comment'], 'trim'],
            ['adequacy_comment', 'required', 'when' => function ($model) {
                return $model->adequacy === self::ADEQUACY_NOT_SURE;
            }],
            ['hesitation', 'required', 'when' => function ($model) {
                return $model->met_requirements === self::REQUIREMENTS_NOT_SURE;
            }],
            ['potential_cooperation_comment', 'required', 'when' => function ($model) {
                return $model->potential_cooperation === self::POTENTIAL_COOPERATION_OTHER;
            }],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_VALIDATE => 'created_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getSkillLevels()
    {
        return [
            static::LEVEL_JUNIOR => Yii::t('feedback', 'Junior'),
            static::LEVEL_JUNIOR_UPPER => Yii::t('feedback', 'Junior upper'),
            static::LEVEL_MIDDLE => Yii::t('feedback', 'Middle'),
            static::LEVEL_MIDDLE_UPPER => Yii::t('feedback', 'Middle upper'),
            static::LEVEL_SENIOR => Yii::t('feedback', 'Senior'),
            static::LEVEL_SENIOR_UPPER => Yii::t('feedback', 'Senior upper'),
        ];
    }

    /**
     * @return array
     */
    public static function getRequirements()
    {
        return [
            self::REQUIREMENTS_YES => Yii::t('feedback', 'Yes'),
            self::REQUIREMENTS_NO => Yii::t('feedback', 'No'),
            self::REQUIREMENTS_NOT_SURE => Yii::t('feedback', 'Not sure'),
        ];
    }

    public static function getPotentialCooperation()
    {
        return [
            self::POTENTIAL_COOPERATION_YES => Yii::t('feedback', 'Yes'),
            self::POTENTIAL_COOPERATION_NO => Yii::t('feedback', 'No'),
            self::POTENTIAL_COOPERATION_OTHER => Yii::t('feedback', 'Other'),
        ];
    }

    /**
     * @return array
     */
    public static function getEnglishLevels()
    {
        return [
            self::ENGLISH_LEVEL_BEGINNER => Yii::t('feedback', 'Beginner'),
            self::ENGLISH_LEVEL_ELEMENTARY => Yii::t('feedback', 'Elementary'),
            self::ENGLISH_LEVEL_PRE_INTERMEDIATE => Yii::t('feedback', 'Pre intermediate'),
            self::ENGLISH_LEVEL_INTERMEDIATE => Yii::t('feedback', 'intermediate'),
            self::ENGLISH_LEVEL_UPPER_INTERMEDIATE => Yii::t('feedback', 'Upper intermediate'),
            self::ENGLISH_LEVEL_ADVANCED => Yii::t('feedback', 'Advanced'),
        ];
    }

    /**
     * @return array
     */
    public static function getAdequacyList()
    {
        return [
            self::ADEQUACY_YES => Yii::t('feedback', 'Yes'),
            self::ADEQUACY_NO => Yii::t('feedback', 'No'),
            self::ADEQUACY_NO_FULLY => Yii::t('feedback', 'Not Fully'),
            self::ADEQUACY_NOT_SURE => Yii::t('feedback', 'Other'),
        ];
    }

}
