<?php

namespace app\models;

use Yii;
use app\models\base\UserBase;
use yii\base\NotSupportedException;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 */
class User extends UserBase implements IdentityInterface
{

    /**
     * @var string Plain password. Used for model validation.
     */
    public $password;

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('\'findIdentityByAccessToken\' is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->getAttribute('auth_key');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->getAttribute('password_hash'));
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function generatePasswordHash()
    {
        $this->setAttribute('password_hash', Yii::$app->getSecurity()->generatePasswordHash($this->password));

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'full_name',
                    self::EVENT_BEFORE_UPDATE => 'full_name',
                ],
                'value' => function () {
                    return implode(' ', [$this->first_name, $this->last_name]);
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'auth_key',
                ],
                'value' => function () {
                    return Yii::$app->getSecurity()->generateRandomString();
                },
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_VALIDATE => 'created_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!$this->getIsNewRecord()) {
            return true;
        }

        if ($this->password) {
            $this->generatePasswordHash();
        }

        return true;
    }

}
