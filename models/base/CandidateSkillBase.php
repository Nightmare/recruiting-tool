<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "candidate_skill".
 *
 * @property integer $candidate_id
 * @property string $skill
 *
 * @property Candidate $candidate
 */
class CandidateSkillBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate_skill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'skill'], 'required'],
            [['candidate_id'], 'integer'],
            [['skill'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'skill' => Yii::t('app', 'Skill'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(\app\models\Candidate::className(), ['id' => 'candidate_id']);
    }
}
