<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "conversation_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $candidate_id
 * @property string $text
 * @property string $created_at
 *
 * @property User $user
 * @property Candidate $candidate
 */
class ConversationHistoryBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conversation_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'candidate_id', 'text', 'created_at'], 'required'],
            [['user_id', 'candidate_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(\app\models\Candidate::className(), ['id' => 'candidate_id']);
    }
}
