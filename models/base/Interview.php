<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "interview".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property integer $vacancy_id
 * @property string $appointment_at
 * @property string $status
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property Feedback[] $feedbacks
 * @property Vacancy $vacancy
 * @property User $user
 * @property Account $account
 * @property InterviewParticipant[] $interviewParticipants
 */
class Interview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interview';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'account_id', 'vacancy_id', 'appointment_at', 'status', 'comment', 'created_at'], 'required'],
            [['user_id', 'account_id', 'vacancy_id'], 'integer'],
            [['appointment_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status', 'comment'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('feedback', 'ID'),
            'user_id' => Yii::t('feedback', 'User ID'),
            'account_id' => Yii::t('feedback', 'Account ID'),
            'vacancy_id' => Yii::t('feedback', 'Vacancy ID'),
            'appointment_at' => Yii::t('feedback', 'Appointment At'),
            'status' => Yii::t('feedback', 'Status'),
            'comment' => Yii::t('feedback', 'Comment'),
            'created_at' => Yii::t('feedback', 'Created At'),
            'updated_at' => Yii::t('feedback', 'Updated At'),
            'deleted_at' => Yii::t('feedback', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(\app\models\Feedback::className(), ['interview_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(\app\models\Vacancy::className(), ['id' => 'vacancy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(\app\models\Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviewParticipants()
    {
        return $this->hasMany(\app\models\InterviewParticipant::className(), ['interview_id' => 'id']);
    }
}
