<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "account".
 *
 * @property integer $id
 * @property string $redmine_account_id
 * @property string $name
 * @property string $type
 * @property string $website
 * @property string $billing_address_street
 * @property string $billing_address_city
 * @property string $billing_address_state
 * @property string $billing_address_postalcode
 * @property string $billing_address_country
 * @property string $shipping_address_street
 * @property string $shipping_address_city
 * @property string $shipping_address_state
 * @property string $shipping_address_postalcode
 * @property string $shipping_address_country
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property Interview[] $interviews
 * @property Vacancy[] $vacancies
 */
class AccountBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redmine_account_id', 'created_at'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['redmine_account_id'], 'string', 'max' => 36],
            [['name', 'website', 'billing_address_country', 'shipping_address_country'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['billing_address_street', 'shipping_address_street'], 'string', 'max' => 150],
            [['billing_address_city', 'billing_address_state', 'shipping_address_city', 'shipping_address_state'], 'string', 'max' => 100],
            [['billing_address_postalcode', 'shipping_address_postalcode'], 'string', 'max' => 20],
            [['redmine_account_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'redmine_account_id' => Yii::t('app', 'Redmine Account ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'website' => Yii::t('app', 'Website'),
            'billing_address_street' => Yii::t('app', 'Billing Address Street'),
            'billing_address_city' => Yii::t('app', 'Billing Address City'),
            'billing_address_state' => Yii::t('app', 'Billing Address State'),
            'billing_address_postalcode' => Yii::t('app', 'Billing Address Postalcode'),
            'billing_address_country' => Yii::t('app', 'Billing Address Country'),
            'shipping_address_street' => Yii::t('app', 'Shipping Address Street'),
            'shipping_address_city' => Yii::t('app', 'Shipping Address City'),
            'shipping_address_state' => Yii::t('app', 'Shipping Address State'),
            'shipping_address_postalcode' => Yii::t('app', 'Shipping Address Postalcode'),
            'shipping_address_country' => Yii::t('app', 'Shipping Address Country'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviews()
    {
        return $this->hasMany(\app\models\Interview::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancies()
    {
        return $this->hasMany(\app\models\Vacancy::className(), ['account_id' => 'id']);
    }
}
