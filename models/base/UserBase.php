<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "user".
 *
 * @property integer $id
 * @property integer $redmine_id
 * @property string $auth_key
 * @property string $login
 * @property string $first_name
 * @property string $last_name
 * @property string $full_name
 * @property string $email
 * @property string $password_hash
 * @property string $created_at
 *
 * @property Candidate[] $candidates
 * @property ConversationHistory[] $conversationHistories
 * @property Interview[] $interviews
 * @property InterviewParticipant[] $interviewParticipants
 */
class UserBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redmine_id', 'full_name', 'created_at'], 'required'],
            [['redmine_id'], 'integer'],
            [['created_at'], 'safe'],
            [['auth_key', 'login', 'first_name', 'last_name', 'email', 'password_hash'], 'string', 'max' => 255],
            [['full_name'], 'string', 'max' => 100],
            [['login'], 'unique'],
            [['redmine_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'redmine_id' => Yii::t('app', 'Redmine ID'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'login' => Yii::t('app', 'Login'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'full_name' => Yii::t('app', 'Full Name'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidates()
    {
        return $this->hasMany(\app\models\Candidate::className(), ['recruiter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversationHistories()
    {
        return $this->hasMany(\app\models\ConversationHistory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviews()
    {
        return $this->hasMany(\app\models\Interview::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviewParticipants()
    {
        return $this->hasMany(\app\models\InterviewParticipant::className(), ['user_id' => 'id']);
    }
}
