<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $candidate_id
 * @property string $action
 * @property string $entity
 * @property string $message
 * @property string $created_at
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'candidate_id', 'action', 'entity', 'message', 'created_at'], 'required'],
            [['user_id', 'candidate_id'], 'integer'],
            [['created_at'], 'safe'],
            [['action'], 'string', 'max' => 30],
            [['entity', 'message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'action' => Yii::t('app', 'Action'),
            'entity' => Yii::t('app', 'Entity'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
