<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "interview_participant".
 *
 * @property integer $interview_id
 * @property integer $candidate_id
 * @property integer $user_id
 *
 * @property Interview $interview
 * @property Candidate $candidate
 * @property User $user
 */
class InterviewParticipantBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interview_participant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interview_id', 'candidate_id', 'user_id'], 'required'],
            [['interview_id', 'candidate_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'interview_id' => Yii::t('app', 'Interview ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterview()
    {
        return $this->hasOne(\app\models\Interview::className(), ['id' => 'interview_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(\app\models\Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
}
