<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "vacancy".
 *
 * @property integer $id
 * @property string $redmine_vacancy_id
 * @property integer $account_id
 * @property string $name
 * @property string $status
 * @property string $importance
 * @property string $location
 * @property string $description
 * @property string $opened_at
 * @property string $started_at
 * @property string $closed_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property Interview[] $interviews
 * @property Account $account
 */
class VacancyBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redmine_vacancy_id', 'account_id', 'created_at'], 'required'],
            [['account_id'], 'integer'],
            [['status', 'importance', 'description'], 'string'],
            [['opened_at', 'started_at', 'closed_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['redmine_vacancy_id'], 'string', 'max' => 36],
            [['name'], 'string', 'max' => 255],
            [['location'], 'string', 'max' => 100],
            [['redmine_vacancy_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'redmine_vacancy_id' => Yii::t('app', 'Redmine Vacancy ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'importance' => Yii::t('app', 'Importance'),
            'location' => Yii::t('app', 'Location'),
            'description' => Yii::t('app', 'Description'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'started_at' => Yii::t('app', 'Started At'),
            'closed_at' => Yii::t('app', 'Closed At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviews()
    {
        return $this->hasMany(\app\models\Interview::className(), ['vacancy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(\app\models\Account::className(), ['id' => 'account_id']);
    }
}
