<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "cv".
 *
 * @property integer $id
 * @property integer $candidate_id
 * @property string $path
 * @property string $created_at
 * @property string $deleted_at
 *
 * @property Candidate $candidate
 */
class CvBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['candidate_id', 'path', 'created_at'], 'required'],
            [['candidate_id'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
            [['path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'path' => Yii::t('app', 'Path'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(\app\models\Candidate::className(), ['id' => 'candidate_id']);
    }
}
