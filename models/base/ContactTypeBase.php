<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "contact_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Contact[] $contacts
 * @property Candidate[] $candidates
 */
class ContactTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(\app\models\Contact::className(), ['contact_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidates()
    {
        return $this->hasMany(\app\models\Candidate::className(), ['id' => 'candidate_id'])->viaTable('contact', ['contact_type_id' => 'id']);
    }
}
