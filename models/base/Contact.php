<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "contact".
 *
 * @property integer $id
 * @property integer $contact_type_id
 * @property integer $candidate_id
 * @property string $name
 *
 * @property ContactType $contactType
 * @property Candidate $candidate
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_type_id', 'candidate_id', 'name'], 'required'],
            [['contact_type_id', 'candidate_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_type_id' => Yii::t('app', 'Contact Type ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactType()
    {
        return $this->hasOne(\app\models\ContactType::className(), ['id' => 'contact_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(\app\models\Candidate::className(), ['id' => 'candidate_id']);
    }
}
