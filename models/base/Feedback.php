<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "feedback".
 *
 * @property integer $id
 * @property integer $interview_id
 * @property string $met_requirements
 * @property string $hesitation
 * @property string $professional_level
 * @property string $adequacy
 * @property string $adequacy_comment
 * @property string $potential_cooperation
 * @property string $potential_cooperation_comment
 * @property string $english_level
 * @property string $general_feedback
 * @property string $created_at
 *
 * @property Interview $interview
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['interview_id', 'met_requirements', 'professional_level', 'adequacy', 'potential_cooperation', 'general_feedback', 'created_at'], 'required'],
            [['interview_id'], 'integer'],
            [['met_requirements', 'hesitation', 'professional_level', 'adequacy', 'potential_cooperation', 'potential_cooperation_comment', 'english_level', 'general_feedback'], 'string'],
            [['created_at'], 'safe'],
            [['adequacy_comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('feedback', 'ID'),
            'interview_id' => Yii::t('feedback', 'Interview ID'),
            'met_requirements' => Yii::t('feedback', 'Met Requirements'),
            'hesitation' => Yii::t('feedback', 'Hesitation'),
            'professional_level' => Yii::t('feedback', 'Professional Level'),
            'adequacy' => Yii::t('feedback', 'Adequacy'),
            'adequacy_comment' => Yii::t('feedback', 'Adequacy Comment'),
            'potential_cooperation' => Yii::t('feedback', 'Potential Cooperation'),
            'potential_cooperation_comment' => Yii::t('feedback', 'Potential Cooperation Comment'),
            'english_level' => Yii::t('feedback', 'English Level'),
            'general_feedback' => Yii::t('feedback', 'General Feedback'),
            'created_at' => Yii::t('feedback', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterview()
    {
        return $this->hasOne(\app\models\Interview::className(), ['id' => 'interview_id']);
    }
}
