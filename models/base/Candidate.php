<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "candidate".
 *
 * @property integer $id
 * @property integer $recruiter_id
 * @property string $first_name
 * @property string $last_name
 * @property string $logo
 * @property string $email
 * @property string $position
 * @property string $birthday
 * @property integer $experience
 * @property string $skills_type
 * @property string $identity_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property \app\models\User $recruiter
 * @property \app\models\CandidateSkill[] $candidateSkills
 * @property \app\models\Contact[] $contacts
 * @property \app\models\ConversationHistory[] $conversationHistories
 * @property \app\models\Cv[] $cvs
 * @property \app\models\InterviewParticipant[] $interviewParticipants
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recruiter_id', 'first_name', 'last_name', 'created_at'], 'required'],
            [['recruiter_id', 'experience'], 'integer'],
            [['birthday', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['skills_type'], 'string'],
            [['first_name', 'last_name', 'logo', 'email', 'identity_id'], 'string', 'max' => 255],
            [['position'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'recruiter_id' => Yii::t('app', 'Recruiter ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'logo' => Yii::t('app', 'Logo'),
            'email' => Yii::t('app', 'Email'),
            'position' => Yii::t('app', 'Position'),
            'birthday' => Yii::t('app', 'Birthday'),
            'experience' => Yii::t('app', 'Experience'),
            'skills_type' => Yii::t('app', 'Skills Type'),
            'identity_id' => Yii::t('app', 'Identity ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecruiter()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'recruiter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateSkills()
    {
        return $this->hasMany(\app\models\CandidateSkill::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(\app\models\Contact::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversationHistories()
    {
        return $this->hasMany(\app\models\ConversationHistory::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCvs()
    {
        return $this->hasMany(\app\models\Cv::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterviewParticipants()
    {
        return $this->hasMany(\app\models\InterviewParticipant::className(), ['candidate_id' => 'id']);
    }
}
