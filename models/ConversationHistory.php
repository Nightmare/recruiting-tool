<?php

namespace app\models;

use Yii;
use app\models\base\ConversationHistoryBase;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "conversation_history".
 */
class ConversationHistory extends ConversationHistoryBase
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'created_at',
                    self::EVENT_BEFORE_VALIDATE => 'created_at',
//                    self::EVENT_BEFORE_UPDATE => 'updated_at',
//                    self::EVENT_BEFORE_DELETE => 'deleted_at',
                ],
                'value' => function () {
                    return new Expression('NOW()');
                }
            ],
        ];
    }

}
