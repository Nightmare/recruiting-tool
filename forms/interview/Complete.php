<?php

namespace app\forms\interview;

use app\models\User;
use yii\base\Model;

/**
 * Class Complete
 * @package app\forms\interview
 */
class Complete extends Model
{

    public $content;

    public $interview_id;

    public $candidate_id;

    public $participants;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'interview-complete-form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'interview_id', 'candidate_id', 'participants'], 'required'],
            [['interview_id', 'candidate_id'], 'integer'],
            ['participants', function ($attribute) {
                if ($this->hasErrors()) {
                    return;
                }

                $participants = array_filter(array_map('trim', explode(',', $this->participants)));
                $this->participants = join(',', $participants);
                $unknown = null;

                foreach ($participants as $participant) {
                    if (!User::findOne(['full_name' => $participant])) {
                        $unknown = $participant;
                        break;
                    }
                }

                if (null !== $unknown) {
                    $this->addError($attribute, \Yii::t('app', "Unknown '{$unknown}' participant"));
                }
            }],
        ];
    }

}