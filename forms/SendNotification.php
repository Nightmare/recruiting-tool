<?php

namespace app\forms;

use app\models\User;
use yii\base\Model;

/**
 * Class Interview
 * @package app\forms
 */
class SendNotification extends Model
{

    public $comment;

    public $candidate_id;

    public $participants;

    public $attach_cv = 0;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'send-notification-form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'participants', 'candidate_id'], 'required'],
            ['comment', 'string'],
            ['candidate_id', 'integer'],
            ['attach_cv', 'boolean'],
            ['participants', function ($attribute) {
                if ($this->hasErrors()) {
                    return;
                }

                $participants = array_filter(array_map('trim', explode(',', $this->participants)));
                $this->participants = join(',', $participants);
                $unknown = null;

                foreach ($participants as $participant) {
                    if (!User::findOne(['full_name' => $participant])) {
                        $unknown = $participant;
                        break;
                    }
                }

                if (null !== $unknown) {
                    $this->addError($attribute, \Yii::t('app', "Unknown '{$unknown}' participant"));
                }
            }],
        ];
    }

} 