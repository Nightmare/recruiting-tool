<?php

namespace app\forms;

use app\models\User;
use yii\base\Model;

/**
 * Class Interview
 * @package app\forms
 */
class Interview extends Model
{

    public $account_id;

    public $interview_id;

    public $vacancy_id;

    public $account;

    public $vacancy;

    public $comment;

    public $appointment_at;

    public $candidate_id;

    public $send_invitation = 1;

    public $attach_cv = 0;

    public $participants;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'interview-form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appointment_at', 'participants', 'candidate_id', 'account_id', 'vacancy_id', 'account', 'vacancy'], 'required'],
            [['candidate_id', 'account_id', 'vacancy_id', 'interview_id'], 'integer'],
            [['appointment_at'], 'date', 'format' => 'php:Y-m-d H:i'],
            [['comment'], 'string'],
            [['interview_id'], 'safe'],
            [['send_invitation', 'attach_cv'], 'boolean'],
            ['participants', function ($attribute) {
                if ($this->hasErrors()) {
                    return;
                }

                $participants = array_filter(array_map('trim', explode(',', $this->participants)));
                $this->participants = join(',', $participants);
                $unknown = null;

                foreach ($participants as $participant) {
                    if (!User::findOne(['full_name' => $participant])) {
                        $unknown = $participant;
                        break;
                    }
                }

                if (null !== $unknown) {
                    $this->addError($attribute, \Yii::t('app', "Unknown '{$unknown}' participant"));
                }
            }],
        ];
    }

} 