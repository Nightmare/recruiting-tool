<?php

namespace app\forms;

use Yii;
use yii\base\Model;
use Redmine\Client as RedmineClient;
use app\models\User;
/**
 * LoginForm is the model behind the login form.
 */
class Login extends Model
{

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var bool Whether to remember the user.
     */
    public $rememberMe = true;

    /**
     * @var User
     */
    protected $_user;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'login-form';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me next time'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['username', 'trim'],
            ['rememberMe', 'boolean'],
            ['password', function ($attribute) {
                if ($this->hasErrors()) {
                    return;
                }

                if (!($user = $this->getUser()) || !$user->validatePassword($this->password)) {
                    $this->addError($attribute, Yii::t('app', 'Invalid login or password'));
                }
            }],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->getUser()->login($this->getUser(), $this->rememberMe ? Yii::$app->params['rememberFor'] : 0);
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            /** @var User $user */
            $user = User::findOne(['login' => $this->username]);
            $user = $this->getRedmineUser($user);

            $this->_user = $user;
        }

        return $this->_user;
    }

    /**
     * @param User|null $user
     * @return User|null
     */
    public function getRedmineUser(User $user = null)
    {
        $redmineClient = new RedmineClient(Yii::$app->params['redmine']['url'], $this->username, $this->password);

        /** @var \Redmine\Api\User $redmineUser */
        $redmineUser = $redmineClient->api('user');
        $redmineUser = $redmineUser->getCurrentUser();

        if (200 !== $redmineClient->getResponseCode() || !isset($redmineUser['user']['id'])) {
            return null;
        }

        $redmineUser = $redmineUser['user'];
        $isUserExist = $user instanceof User;

        if (!$isUserExist) {
            $user = new User();
        }

        $user->setAttributes([
            'redmine_id' => $redmineUser['id'],
            'email' => $redmineUser['mail'],
            'first_name' => $redmineUser['firstname'],
            'last_name' => $redmineUser['lastname'],
            'login' => $redmineUser['login'],
            'created_at' => (new \DateTime($redmineUser['created_on']))->format('Y-m-d H:i:s'),
        ]);
        $user->password = $this->password;

        if ($isUserExist && $user->password) {
            $user->generatePasswordHash();
        }

        return $user->save() ? $user : null;
    }

}
