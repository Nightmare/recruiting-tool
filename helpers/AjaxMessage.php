<?php

namespace app\helpers;

use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Class AjaxMessage
 * @package app\helpers
 */
class AjaxMessage
{

    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';

    /**
     * @var array
     */
    protected $_messages = [
        self::TYPE_ERROR => [],
        self::TYPE_WARNING => [],
        self::TYPE_SUCCESS => [],
        self::TYPE_INFO => [],
    ];

    /**
     * @param $message
     * @param string $type
     * @return $this
     */
    public function addMessage($message, $type = self::TYPE_SUCCESS)
    {
        if (array_key_exists($type, $this->_messages)) {
            $this->_messages[$type][] = $message;
        }
        return $this;
    }

    /**
     * @param string $message
     * @return AjaxMessage
     */
    public function addSuccessMessage($message)
    {
        return $this->addMessage($message, self::TYPE_SUCCESS);
    }

    /**
     * @param string $message
     * @return AjaxMessage
     */
    public function addErrorMessage($message)
    {
        return $this->addMessage($message, self::TYPE_ERROR);
    }

    /**
     * @param Model $model
     * @return $this
     */
    public function addModelErrors(Model $model)
    {
        $rawErrors = [];

        foreach ($model->getErrors() as $field => $errors) {
            $rawErrors += $errors;
        }

        $this->_messages[self::TYPE_ERROR] += $rawErrors;
        return $this;
    }

    /**
     * @param \Exception $exception
     * @return $this
     */
    public function addExceptionError(\Exception $exception)
    {
        $this->_messages[self::TYPE_ERROR][] = $exception->getMessage();
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return array_filter($this->_messages);
    }

} 