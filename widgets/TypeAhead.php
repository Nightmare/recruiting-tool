<?php

namespace app\widgets;

use kartik\typeahead\TypeaheadAsset;
use yii\helpers\Json;
use dosamigos\typeahead\Bloodhound;
use yii\web\JsExpression;

/**
 * Class TypeAhead
 * @package app\widgets
 */
class TypeAhead extends \dosamigos\typeahead\TypeAhead
{

    /**
     * Events
     * @var array
     */
    public $events = [];

    /**
     * @inheritdoc
     */
    protected function registerClientScript()
    {
        $view = $this->getView();

        // changed only this line
        TypeaheadAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : 'null';

        foreach($this->dataSets as $dataSet) {
            if(empty($dataSet)) {
                continue;
            }
            $dataSets[] = Json::encode($dataSet);
        }

        $dataSets = !empty($dataSets)
            ? implode(", ", $dataSets)
            : '{}';

        foreach ($this->engines as $bloodhound) {
            if ($bloodhound instanceof Bloodhound) {
                $js[] = $bloodhound->getClientScript();
            }
        }

        $js[] = "jQuery('#$id').typeahead($options, $dataSets);";
        if (!empty($this->events) && is_array($this->events)) {
            foreach ($this->events as $event => $jsFunction) {
                if ($jsFunction instanceof JsExpression) {
                    $js[] = "jQuery('#$id').on('typeahead:$event', $jsFunction);";
                }
            }
        }

        $view->registerJs(implode("\n", $js));
    }

} 