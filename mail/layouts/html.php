<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
    /* Template styling */
    html, body {
        height: 100%;
    }

    body {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        width: 100%;
        max-width: 100%;
        font-size: 17px;
        line-height: 24px;
        color: #42464D;
        background: #F9F9F9;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75), 0 1px 1px white;
    }

    table {
        width: 100%;
        margin: 0 auto;
    }

    h1, h2, h3, h4 {
        color: #2ab27b;
        margin-bottom: 12px;
        line-height: 26px;
    }

    p, ul, ul li {
        font-size: 17px;
        margin: 0 0 16px;
        line-height: 24px;
    }

    p.mini {
        font-size: 12px;
        line-height: 18px;
        color: #ABAFB4;
    }

    p.message {
        font-size: 16px;
        line-height: 20px;
        margin-bottom: 4px;
    }

    hr {
        margin: 2rem auto;
        width: 50%;
        border: none;
        border-bottom: 1px solid #DDD;
    }

    a, a:link, a:visited, a:active, a:hover {
        font-weight: bold;
        color: #439fe0;
        text-decoration: none;
        word-break: break-word;
    }

    .time {
        font-size: 11px;
        color: #ABAFB4;
        padding-right: 6px;
    }

    .emoji {
        vertical-align: bottom;
    }

    .avatar {
        border-radius: 2px;
    }

    #footer p {
        margin-top: 16px;
        font-size: 12px;
    }

    /* Client-specific Styles */
    #outlook a {
        padding: 0;
    }

    body {
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0 auto;
        padding: 0;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
    }

    #backgroundTable {
        margin: 0;
        padding: 0;
        width: 100%;
        line-height: 100% !important;
    }

    /* Some sensible defaults for images
    Bring inline: Yes. */
    img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
    }

    a img {
        border: none;
    }

    .image_fix {
        display: block;
    }

    /* Outlook 07, 10 Padding issue fix
    Bring inline: No.*/
    table td {
        border-collapse: collapse;
    }

    /* Fix spacing around Outlook 07, 10 tables
    Bring inline: Yes */
    table {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    /* Mobile */
    @media only screen and (max-device-width: 480px) {
        /* Part one of controlling phone number linking for mobile. */
        a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: blue; /* or whatever your want */
            pointer-events: none;
            cursor: default;
        }

        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: orange !important;
            pointer-events: auto;
            cursor: default;
        }

    }

    /* More Specific Targeting */
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        /* You guessed it, ipad (tablets, smaller screens, etc) */
        /* repeating for the ipad */
        a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: blue; /* or whatever your want */
            pointer-events: none;
            cursor: default;
        }

        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: orange !important;
            pointer-events: auto;
            cursor: default;
        }
    }

    /* iPhone Retina */
    @media only screen and (-webkit-min-device-pixel-ratio: 2) and (max-device-width: 640px) {
        /* Must include !important on each style to override inline styles */
        #footer p {
            font-size: 9px;
        }
    }

    /* Android targeting */
    @media only screen and (-webkit-device-pixel-ratio: .75) {
        /* Put CSS for low density (ldpi) Android layouts in here */
        img {
            max-width: 100%;
            height: auto;
        }
    }

    @media only screen and (-webkit-device-pixel-ratio: 1) {
        /* Put CSS for medium density (mdpi) Android layouts in here */
        img {
            max-width: 100%;
            height: auto;
        }
    }

    @media only screen and (-webkit-device-pixel-ratio: 1.5) {
        /* Put CSS for high density (hdpi) Android layouts in here */
        img {
            max-width: 100%;
            height: auto;
        }
    }

    /* Galaxy Nexus */
    @media only screen and (min-device-width: 720px) and (max-device-width: 1280px) {
        img {
            max-width: 100%;
            height: auto;
        }

        body {
            font-size: 16px;
        }
    }
    /* end Android targeting */
    </style>
</head>
<body>
    <?php $this->beginBody() ?>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="font-size: 17px; line-height: 24px; color: #42464D; background: #F9F9F9; text-shadow: 0 1px 1px rgba(255,255,255,0.75), 0 1px 1px white; height: 100%;">
        <tr style="height: 1px">
            <td valign="top">
                <table id="header" width="100%" cellpadding="0" cellspacing="0" border="0" style="background: white; border-bottom: 1px solid #DDDDDD;">
                    <tr>
                        <td valign="bottom" style="padding: 20px 16px 16px;">
                            <div style="max-width: 600px; margin: 0 auto;">
                                <a href="http://brightgrove.com">
                                    <img alt="Brightgrove Ltd" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANAAAAAdCAYAAADb0uIeAAAPM0lEQVR4Ae1cC5hVVRW+WZaSyYgSiuIjk7QsrSmtRuVy58ydx1wlH6OmpJL5IL7E1PzK/Eoys89EzcAEUnkQwshDYR7DPGZ4MEDEA+mTMEgkkTfIPEAE4fb/l738Fttz97nQOAx2+L71nXvO3mettdde/15rr32GyKBBg3zJ87z8vLy8nV5ubjI3FktRQX5+Mjc3d+j+PiGFFFLaBgCmgoAR8JDy4/GkF43e4GAYUkghgHJyck4BYFryPO8D8JjfWxCZujgYhhRSCCBEmR/a0cfcvxjAMKSQQgDFYrFypmt2+obn13QEpfHvKC8n53REQ9IZjJjtJTuRk9O1oKDgbMj9AugcUKe2lsEoD+qBcfXg+EDHHAqf7OzsY8kHdFqKT3b28W2ra0h+k9cdhYOWvNxcO33bjLasjqA0gFwQz8trYZqJyNiKwsYj7SG3pKTkWMheAdm0yT5jk8+3MXiOAd9lcc9rxjw05/bu/cahps2x3r2fhn2awY+8WpBZXNy2NgnJzzlv9Uvf8PyvHUVp6DSisKAgBex8ODMcLNoecqPR6Lcgcx2ccR1ssh7OPfIjWBzyGO05tqLCwkNOm2GT48BrA3mZ+XwdkfNTH7GNQgDBKSr90jdMyFUdQWGmJdBpTRwOxkiA38vbmj/G2hmUlUgkTuC9X7/S0tKjkEp+oi1kko8C0HDj8BzfPoA291B4ItpcIfNIfrFo9BcuuTYRbBnSZ2iLIDCLTUFdEMk/2VEA8L/qYg/0VKRDrXb1DanAJhrATPC9eNaAay0JfepwX4uJ/hLbk9MiFyUbIhXJykh9siJSm6KZkRnJ8sgdqfaaSCc8ey5ZFWnAs5oUTUffysho0KczWKF7G+AkzRnVLoD+77guMrSYZJ7VYTxToPvD0O/MNBXHrnFEXfQZ58ViK/HOO6BmUAv4N+G9iwRYkD3Bw1jxvIaRgc5jOW0fyBuCtmqjx3LQCoteAzVC1hwS+s+DjJ7iaGh7m+MT20PmP/H7VUXLQEtBc9FGXcaDz0C8+1krSo8RIHKxQb//CA/8XkbZOv3kPhLP7kfqXsF29FsL2ohnmzzMvxDsSj7LSdBvOfpW2XYA36/ivV+CVzXG9xb6NtGm6N+C5/N1f/Qthn5P4nk5aCH1symG55DbAF5TYZvfwc5fVP7wPPjOgx5z0V4Nft0C/Odp8JgP3Ro5D+h/lmq7HDye4hEOfcnH7ouBhRnQcwbOSEcSLwcwR8NtaapvY9hO5fB7D9Mnrm4k/obQ9zEB3dkH4JiSXBxJJmcqwj2AMoDtAMmVyQW4n6XaF6Xa5ydLI66VTHR8UnSE0Sh7ogcj4jpSCG0vgGaZ9EVSodUEi+aFVflWGPPtIoyB4wCf6TDiA6ABGGt/3PeTDTyexWXcieJi8puqnKAzdCmjLPYpLiqiXltAZaBnQUPx/jNwgiGgkQQG+Ihea5SMy/MVeHBd4O1/7y9CuB8OfuMJ8ELIo0zqg+d/VmA4gUcO5EGiDOogPPDsefR/SFZfOGQOnq2l7jJG9JuH956CTr9C3weFuHBQJkHOcdLWIpfAoL4Yz262wSZ72Y5n94FPfzwfAFn5YjMCQuxg7L/QzN0I6skrAULnpUz2LTY2S2CM5MMFiLLYTn+IKnDZRB30HHJsUrThwqF1Ad8FoovYHb/r2Uf0IJDs9K0qTfXte0aBW9LsjyaY6HIigPAOrslkhaFqUGVkOyJTVwOwscnZbFOEe7TfFgSeu+6662jm8pg8iYzrSyPpQUdHk/HgSuNeJm0yFvBiG43xk6B9F/prXlcou42l4TGBwusPjCZp9lGXos9esR3e/Y1v1IBe0DFmv6/AX8A+wodgVW3XybiNY/7cft8C21pxQDhSC8Qm0vWHvoO0jlHP6yXpINom0g5mfrbCBuc75mZUsbEZCzKQ+SNXiivzzv4kyXgMoCUb2QuAnpvG7hdgbDshizah78iePsWj6EBd+jn84CUZP3lpQ/bAQ/l0R6+CG5G+fM68PK3AB2BQ7lq2Azw3MLLY4MDzKWxHhOmMZ5uStR8CWCsAdGoGG+NvUyfqiEHQMUYHRKtSZdzdKlXqDCNt4oSQYMx/uPhw/CwcyIoOR1knAMH166KTWWxqXbw4cSqC7uPEKkfeTF4GGGvs/ZcmLmrWHud+1TZJxg3a4/XqdZ6Dz71wKF8+NpWY/aey26qePXseLamYjKtw/9w86ijGfIVjB0m0q8ygMvmmss1q2KaTAfQfZawkpo9+7wOwr0r0YIoqKS+uF+r5Y2YRMH+T0E+wsUob8g47umgn9dxfJ2QZAE2yowsARYD0Ne1XM2Wz2/G8Igg8xliP6CiA1ebqtCtrIqEdkpO0RFIWjPV68HFusO29jXZWbvSVToPFASkHzlEUsNpvUY6wWDnVtVqGh1w9ICKOUY6zO2pWXu5r0LZdKpTM8wNW9sXoJxF9m6tkzvRLRz1cH/dbrMBjL6NPpvPIsQdVJkWubX+m3BaAvunz/jNSteXRAOR9Wdo4BrRpn+rjAP43wGMX5TBiwW9+rCek2k7fNEPuB1xfJyCCnARAHJC+md/b2WYANM4vfcPzW4PAw0oPDLFUoX+ra7Ix2Kssh/yttHnWCg0+5zoBhBTNSq3y+JyrL1chSSn1fsYmAa4FxAeUU03UqYGkmzZZ0UpW8Nl2aioyMH8/ddjoAokEmRxVcC8gdqNsZgR8nvC8k1h8EX24d3NV91iAgM2Czxf9K5OU6+l9rKXTJda7JdRJFhQBq8wfC0cqor7FfZzjCGOl7JPAdwbHIiHudKZvPtFloyfpG3Js19cJiDI3+qZvZZHJJn3LwrPNPvujFgCsewZl2QsgUyY7VTwImOxRlkNeLCs03uUKLfrPcfFBKVtHDfZfLSABz156ZWSRICBqTLaAe56J7hI1JDL9S1Iji+w9jkTQgQd8BIw2I+M9fjHh2IcM4vv2XteP6Afccyo7vKYiel/I1AvD/Y55/C54aJuNDSqBM2VWmcQb2skxB33wXAPIU++egX3PNrYX7veZxyxQXGZVdEmvg1YJmcrsatonsb8wsg98R4GOJw9Zke90pG/ycWmrEqKjQJYpDkz2Td+wLzIAuyZN+lYWAB5xGoZqrdsPnEZHOViMLoeIskJbUeDugNXvOi2XZVAVyf6kQeoBUA6durEsrnLtRjW2m8FHO9VjTqfC4qGAuAtjO9PIOA33O5SMetf5B/d+KhKs514v0z0X7h9WoJ1q7zUdfJ6w0vArDuY8i5VBy66XGDsIsBMqPa3HO+IvtfZ5FecSbRpALxBkTMttYgYDupeRSPOQ1brGL7qgwtJHhUndLhM93qRvXQGIpoD0bXya9K1fhvufeSp9a4HhTsk0V2fpWzl9OfjoFfqsAABNwuqqI9mlfM5NLFM2nAcISFe4ooakwHZqZUV3kXGRA4gSrSR9q1Ey+ltg759pQQZ9RwTY4YDiB/S/UD794uKqQDsroBjwbwXatVzs3POu0mefFI17P7S9rwB0jfHZX/O9fMrCWZT1vaSc670p88fzLd2eKXGz3RUKvOsTXTbIiuSl/7i0xESXvr7RB1HJAOwE/N5ip2+4tuJZ4IegXNG4sqlJchYduJLovBjvZ5tUqYd7hfZ11ibysEESx+Ct9G1wgE4VCri7oNPZcngN+1InkbHM72Tfsce5XcmoExk8uCRvB6AftiJB2uIH7NbFKn4s4gpvnx3i6jwOYMqkK16IgHV87pKLvh/srRgxGTnTzJH4ZCEra9BzLygJgOyx95N+8yfnaAdLqXzaN7rgQFKvMAHVt5cBGL/q242m/Vrf9K0yw/QNaZaebOrs6JuA0Xeqb8nGpFuheR+QNt5slWZ/r3jdh+pN4F/qqgPonQq4dUrfOy2dHnLpZEXQ1hzMj+T74MEKkXx5sCbdhljSLqv6dLtrzyVRuNAufqD4pED7LvVw8BkoMg2QNjD9TGOzTtxCSNaB93z/mJPlaO5RJKrxzAtzsQTvSWQdmLZ8j3b3/AUTDTCXB1oQKCRVhsuNoHtwaqvb5RS31ByenpGsAhga1ZcFc1Op2XsqfZtwqOkbV2OueDxtpmyJQDyIIzhI+D0WhnuF1R358sAcaE5iFJWcX/hwMnhKDufr4SzxorrFscopuM5/ecgpE8t22dfw8JYHy4rGMdLpjTNBI3+WwdN3OUk3ZfCvOYB4jsikXvr8hKVZvs82cR5uuNG/jHm9vXLzawv5coJkNuJLoONM0CxFdaDVlGsXPwpwmMn3yIdjYN+AiB7VOhbhmkr/YCfoOExO/WOYS35CJHMF2bvx7CFHKX6FzDl1LDQ+4OHMzREN89Ff5k8i69+YqtK3cB2tKR6Pj2HZ2uaTyvE5cObSJCjQwEFk49N9s2oMQB7ZIO3si/sy1sQNgM7Ht2w1qW/eynGtBNVH6vB7oGnvgt/bfKtvGaRvLFVD7ssw4nQMrAq/qwh6fuumiaVTPgdNRb/BeC/f/uYN/cr4fRZW6Ho6VYDcLPCawu/e8A7t8xwd3k5J0O8J8zlRuXG4RiHzrdts2aQaJ9yRwGbfyDiRh9PUycgYFhCJ4+hHB6/GhNdhRS7WfyPF0j3GNpw82Yf2QD+C4vF03xVyc0znZdWSp+z0ByHeo60Geuu0vVG9X4gUaYbSp08G6Xgu3hnCuYjhPdoM14Vc3ISQqvF+Fsb6Imz0oBw2pyPOJeWbMXNu6zwsXM79lXzN4XlDjS51BBAXNJuoE/gvRZ/bAr7GbnsCUK73S+8AqmkH86Vye1MbyZX0jX9fJdGn3JbRkQn6Xkng2MWPkNoJQADMS37pG4B1y/+DgeFwP7O+erjxSNJfKrR5pvghe5x2oBBATN8AGDt947UZ+6OTP/bgwSEmnI7nUQQPc/KVLH8fQeC/Cbrz8Jp7HO6pnm1P+SGAKiPX+aVvuL5yBIOiG+gGpDbfB13vQ31B/XnIyhKsbNLhiJv5ZwOHW3/sJ74D/e9hdcqH7mZ1EfQo//5GNtkk7n3lb8LaiUIAASilaT4uvelINRidDBWZPSwZ+xIOZ3HdZwoHW0FL4ig0yLnP4SZurs3/J9FkE9p4bTb3W0ArkLpNBnD6hX8O3s4A4t8G2emb+d2Ea7cjOAKdCOoOOjmAuvGPvjqg/sd5SCP5pwl+lG1Ivvc7XBQCCFHmQ+nbnNR1amj4kEIABadvwwxgQKDpoLrIDly90PAfFwrpv/LHdLMx+tnzAAAAAElFTkSuQmCC" style="width: 208px; height: 29px;" />
                                </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr style="height: 100%">
            <td valign="top">
                <table id="body" width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td valign="top">
                            <div style="max-width: 600px; margin: 1.5rem auto 0; padding: 0 16px;"><?= $content ?></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr style="height: 1px">
            <td>
                <table id="footer" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top: 3rem; background: white; color: #989EA6;">
                    <tr>
                        <td style="height: 5px; background-image: url('https://slack.global.ssl.fastly.net/22423/img/email-ribbon_@2x.png'); background-repeat: repeat-x; background-size: auto 5px;"></td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 16px 8px;">
                            <div style="max-width: 600px; margin: 0 auto;">
                                <p class="footer_address" style="margin-top: 16px; font-size: 12px; line-height: 20px;">
                                    Made by <a href="mailto:izubok@brightgrove.com" style="font-weight: bold; color: #439fe0;">Ivan Zubok</a>
                                    &nbsp;&bull;&nbsp; PHP Software Engineer at <a href="http://brightgrove.com" style="font-weight: bold; color: #439fe0;">Brightgrove Ltd</a>
                                </p>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
