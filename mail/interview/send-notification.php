<?php
use yii\helpers\Html;
/** @var string|null $comment */
?>
<p class="message" style="font-size: 13px; font-weight: normal;"><?= Html::encode($comment); ?></p>
