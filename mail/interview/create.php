<?php
use yii\helpers\Html;
/** @var string|null $comment */
/** @var int $isAttachCv */
/** @var string $appointmentAt */
?>
<h2>Hi, colleague(s).</h2>

<p>You have a new interview at <span class="time" style="font-weight: normal; color: #9393a6; padding: 0 4px;"><?= (new DateTime($appointmentAt))->format('Y, d M, H:i'); ?></span></p>

<?php if ($comment) :?>
<p class="message" style="font-size: 13px; font-weight: normal;"><?= Html::encode($comment); ?></p>
<?php endif ?>

<?php if ($isAttachCv) : ?>
<hr />
<p style="font-size: 0.9rem; line-height: 20px; color: #AAA; margin: 0 auto 1rem; word-break: break-word; text-align: left;">
    Also, you can download and skim candidate's CV(s).
</p>
<?php endif; ?>